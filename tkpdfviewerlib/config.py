# -*- coding: utf-8 -*-
"""
tkPDFViewer - Basic PDF viewer
Copyright 2021 Juliette Monsel <j_4321 at protonmail dot com>

tkPDFViewer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tkPDFViewer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Config dialog
"""


import tkinter as tk
from tkinter import ttk
from tkinter import font as tkfont
import re

import fitz

from .constants import CONFIG, LANGUAGES, REV_LANGUAGES, \
    askcolor
from .messagebox import showinfo
from .autocomplete import AutoCompleteCombobox


class FontFrame(ttk.Frame):
    def __init__(self, master, font, style=False, sample_text=_("Sample text")):
        ttk.Frame.__init__(self, master)

        # entry validation
        self._validate_size = self.register(self._validate_font_size)

        # chooser
        self.font = tkfont.Font(self, font=font)
        sample = ttk.Label(self, text=sample_text,
                           anchor="center", font=self.font,
                           style="white.TLabel", relief="groove")

        sample.grid(row=2, columnspan=2, padx=4, pady=6,
                    ipadx=4, ipady=4, sticky="eswn")
        self.fonts = list(set(tkfont.families()))
        self.fonts.append("TkDefaultFont")
        self.fonts.sort()

        prop = self.font.actual()
        self.font_family = tk.StringVar(self, value=prop['family'])
        self.font_family.trace_add('write',
                                   lambda *args: self.font.configure(family=self.font_family.get()))
        self.font_size = tk.StringVar(self, value=prop['size'])
        self.font_size.trace_add('write',
                                 lambda *args: self._config_size(self.font_size, self.font))

        w = max([len(f) for f in self.fonts])
        sizes = list(range(6, 17)) + list(range(18, 32, 2))
        if not prop['size'] in sizes:
            sizes.append(prop['size'])
        sizes.sort()
        self.sizes = ["%i" % i for i in sizes]

        self.choose_family = AutoCompleteCombobox(self, values=self.fonts,
                                                  width=(w * 2) // 3,
                                                  textvariable=self.font_family,
                                                  exportselection=False)
        self.choose_family.current(self.fonts.index(prop['family']))
        self.choose_family.grid(row=0, column=0, padx=4, pady=4)
        self.choose_size = ttk.Combobox(self, values=self.sizes, width=5,
                                        exportselection=False,
                                        textvariable=self.font_size,
                                        validate="key",
                                        validatecommand=(self._validate_size, "%d", "%P", "%V"))
        self.choose_size.current(self.sizes.index(str(prop['size'])))
        self.choose_size.grid(row=0, column=1, padx=4, pady=4)

        if style:
            frame_style = ttk.Frame(self)
            frame_style.grid(row=1, columnspan=2, pady=6)
            self.bold = tk.StringVar(self, value=prop['weight'])
            self.bold.trace_add('write',
                                lambda *args: self.font.configure(weight=self.bold.get()))
            self.italic = tk.StringVar(self, value=prop['slant'])
            self.italic.trace_add('write',
                                  lambda *args: self.font.configure(slant=self.italic.get()))
            self.underline = tk.BooleanVar(self, value=prop['underline'])
            self.underline.trace_add('write',
                                     lambda *args: self.font.configure(underline=self.underline.get()))
            ttk.Checkbutton(frame_style, text=_("Bold"),
                            onvalue='bold', offvalue='normal',
                            variable=self.bold).pack(side='left', padx=4)
            ttk.Checkbutton(frame_style, text=_("Italic"),
                            onvalue='italic', offvalue='roman',
                            variable=self.italic).pack(side='left', padx=4)
            ttk.Checkbutton(frame_style, text=_("Underline"),
                            variable=self.underline).pack(side='left', padx=4)

    def get(self):
        font = self.font.actual()
        props = [font['family'].replace(' ', r'\ '), str(font['size'])]
        if font['weight'] == 'bold':
            props.append("bold")
        if font['slant'] == 'italic':
            props.append("italic")
        if font['underline']:
            props.append("underline")
        if font['overstrike']:
            props.append("overstrike")
        return " ".join(props)

    @staticmethod
    def _config_size(variable, font):
        size = variable.get()
        if size:
            font.configure(size=size)

    def _validate_font_size(self, d, ch, V):
        """Validation of the size entry content."""
        if d == '1':
            l = [i for i in self.sizes if i[:len(ch)] == ch]
            if l:
                i = self.sizes.index(l[0])
                self.choose_size.current(i)
                index = self.choose_size.index("insert")
                self.choose_size.selection_range(index + 1, "end")
                self.choose_size.icursor(index + 1)
            return ch.isdigit()
        else:
            return True


class OpacityFrame(ttk.Frame):
    def __init__(self, master=None, value=0.85, **kw):
        ttk.Frame.__init__(self, master, **kw)

        self.opacity_scale = ttk.Scale(self, orient="horizontal", length=150,
                                       from_=0, to=100,
                                       value=int(100*value),
                                       command=self.display_label)
        self.opacity_label = ttk.Label(self,
                                       text="{val}%".format(val=self.opacity_scale.get()))
        self.opacity_scale.pack(fill='x', expand=True, padx=(4, 40), pady=4)
        self.opacity_label.place(in_=self.opacity_scale, relx=1, rely=0.5,
                                 anchor="w", bordermode="outside")

    def get(self):
        return str(self.opacity_scale.get() / 100)

    def get_opacity(self):
        return self.opacity_scale.get() / 100

    def display_label(self, value):
        self.opacity_label.configure(text="{val}%".format(val=int(float(value))))


class IconSelection(ttk.Combobox):
    icons = {  # Popup icon types
        _('Note'): 'Note',
        _('Comment'): 'Comment',
        _('Key'): 'Key',
        _('Help'): 'Help',
        _('Paragraph'): 'Paragraph',
        _('NewParagraph'): 'NewParagraph',
        _('Insert'): 'Insert',
        _('Graph'): 'Graph',
        _('PushPin'): 'PushPin',
        _('Paperclip'): 'Paperclip',
        _('Tag'): 'Tag',
        _('Mic'): 'Mic',
        _('Speaker'): 'Speaker',
        _('Star'): 'Star',
        _('Unknown'): 'Unknown'
    }

    def __init__(self, master, icon, **kw):
        ttk.Combobox.__init__(self, master, values=list(self.icons.keys()),
                              state="readonly", **kw)
        if icon not in self.icons:
            self.set(_('Unknown'))
        else:
            self.set(icon)

    def get(self):
        return self.icons[ttk.Combobox.get(self)]


class LineEndSelection(ttk.Combobox):
    endings = {  # ending types
        _('None'): fitz.PDF_ANNOT_LE_NONE,
        _('Open arrow'): fitz.PDF_ANNOT_LE_OPEN_ARROW,
        _('Closed arrow'): fitz.PDF_ANNOT_LE_CLOSED_ARROW,
        _('Reversed open arrow'): fitz.PDF_ANNOT_LE_R_OPEN_ARROW,
        _('Reversed closed arrow'): fitz.PDF_ANNOT_LE_R_CLOSED_ARROW,
        _('Butt'): fitz.PDF_ANNOT_LE_BUTT,
        _('Square'): fitz.PDF_ANNOT_LE_SQUARE,
        _('Diamond'): fitz.PDF_ANNOT_LE_DIAMOND,
        _('Circle'): fitz.PDF_ANNOT_LE_CIRCLE,
        _('Slash'): fitz.PDF_ANNOT_LE_SLASH,
    }

    r_endings = {value: key for key, value in endings.items()}

    def __init__(self, master, ending, **kw):
        ttk.Combobox.__init__(self, master, values=list(self.endings.keys()),
                              state="readonly", **kw)
        self.set(self.r_endings.get(ending, _("None")))

    def get(self):
        return self.endings[ttk.Combobox.get(self)]


class AlignSelection(ttk.Combobox):
    align = {  # ending types
        _('Left'): fitz.TEXT_ALIGN_LEFT,
        _('Right'): fitz.TEXT_ALIGN_RIGHT,
        _('Center'): fitz.TEXT_ALIGN_CENTER,
        _('Justify'): fitz.TEXT_ALIGN_JUSTIFY,
    }

    r_align = {value: key for key, value in align.items()}

    def __init__(self, master, align, **kw):
        ttk.Combobox.__init__(self, master, values=list(self.align.keys()),
                              state="readonly", **kw)
        self.set(self.r_align.get(align, _("Left")))

    def get(self):
        return self.align[ttk.Combobox.get(self)]


class ZoomSelection(ttk.Combobox):
    zoom = {  # ending types
        _('Fit width'): "width",
        _('Fit page'): "page",
    }
    zoom.update({str(zoom): str(zoom) for zoom in [50, 75, 100, 125, 150, 200, 300, 400]})

    r_zoom = {value: key for key, value in zoom.items()}

    def __init__(self, master, zoom, **kw):
        ttk.Combobox.__init__(self, master, values=list(self.zoom.keys()), **kw)
        self.set(self.r_zoom.get(zoom, zoom))

    def get(self):
        zoom = ttk.Combobox.get(self)
        if zoom in self.zoom:
            return self.zoom[zoom]
        try:
            float(zoom)
        except ValueError:
            return CONFIG.get("General", "default_zoom")
        return zoom


class ColorFrame(ttk.Frame):
    def __init__(self, master, color='white'):
        ttk.Frame.__init__(self, master)
        frame = ttk.Frame(self, borderwidth=1, relief='raised', padding=2)
        self.preview = tk.Frame(frame, bg=color)
        self.button = ttk.Button(self, image='img_color', padding=2,
                                 command=self.askcolor)
        self.button.pack(side='right', pady=4)
        self.preview.pack(fill='both', expand=True)
        frame.pack(side='left', fill='both', expand=True, padx=(0, 8), pady=4)

    def askcolor(self):
        try:
            color = askcolor(self.preview.cget('bg'), parent=self, title=_('Color'))
        except tk.TclError:
            color = askcolor(parent=self, title=_('Color'))
        if color is not None:
            self.preview.configure(bg=color)

    def get_color(self):
        """Return the color as (html, (r,g,b)) tuple."""
        col = self.preview.cget('bg')
        vmax = self.winfo_rgb('white')[0]
        return col, tuple((v/vmax) for v in self.winfo_rgb(col))

    def get(self):
        """Return the color as "r,g,b" string."""
        col, rgb = self.get_color()
        return ",".join([str(v) for v in rgb])


class OptionalColorFrame(ColorFrame):
    def __init__(self, master, variable, color='white'):
        ColorFrame.__init__(self, master, color)
        self.variable = variable
        self.toggle_state()

    def toggle_state(self):
        if self.variable.get():
            self.button.state(["!disabled"])
        else:
            self.button.state(["disabled"])

    def get_color(self):
        if self.variable.get():
            return ColorFrame.get_color(self)
        else:
            return "", []

    def get(self):
        if self.variable.get():
            return ColorFrame.get(self)
        else:
            return ""


class Config(tk.Toplevel):
    annot_types = sorted(['text', 'freetext', 'rectangle', 'circle', 'polygon',
                          'line', 'arrow', 'polyline', 'ink', 'caret'])

    def __init__(self, master):
        tk.Toplevel.__init__(self, master)
        self.title(_("Settings"))
        self.grab_set()
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)
        self.resizable(True, False)

        self._is_float = self.register(self._validate_float)
        self._is_int = self.register(self._validate_int)

        self.notebook = ttk.Notebook(self)

        # language
        self.lang = tk.StringVar(self, LANGUAGES[CONFIG.get("General", "language")])

        # annots
        self.annot_props = {f"Annot-{annot_type}": {} for annot_type in self.annot_types}


        self._init_general()
        self._init_annots()

        self.notebook.grid(sticky='ewsn', row=0, column=0, columnspan=2)
        ttk.Button(self, text=_('Ok'), command=self.ok).grid(row=1, column=0,
                                                             sticky='e', padx=4,
                                                             pady=10)
        ttk.Button(self, text=_('Cancel'), command=self.destroy).grid(row=1, column=1,
                                                                      sticky='w',
                                                                      padx=4,
                                                                      pady=10)

    def _init_general(self):
        frame_general = ttk.Frame(self)
        self.notebook.add(frame_general, text=_("General"))
        frame_general.columnconfigure(1, weight=1)
        row = 0
        ttk.Label(frame_general, font="TkDefauktFont 9 bold",
                  text=_("Main")).grid(row=row, column=0, padx=4, pady=4,
                                       columnspan=2, sticky="wn")
        # --- Language
        row += 1
        ttk.Label(frame_general, text=_("Language:")).grid(row=row, column=0,
                                                           padx=(12, 4),
                                                           pady=4, sticky="e")

        menu_lang = tk.Menu(frame_general)
        mb = ttk.Menubutton(frame_general, menu=menu_lang, width=9,
                            textvariable=self.lang)
        mb.grid(row=row, column=1, padx=4, pady=4, sticky="w")
        width = 0
        for lang in LANGUAGES:
            language = LANGUAGES[lang]
            width = max(width, len(language))
            menu_lang.add_radiobutton(label=language, value=language,
                                      variable=self.lang, command=self.translate)
        mb.configure(width=width)
        # --- Zoom
        row += 1
        ttk.Label(frame_general, text=_("Default zoom:")).grid(row=row, column=0,
                                                               padx=(12, 4),
                                                               pady=4, sticky="e")
        self.zoom = ZoomSelection(frame_general,
                                  CONFIG.get("General", "default_zoom"))
        self.zoom.grid(row=row, column=1, padx=4, pady=4, sticky="w")
        row += 1
        ttk.Separator(frame_general,
                      orient="horizontal").grid(row=row, columnspan=2,
                                                sticky="ew", padx=8, pady=4)
        # --- Popups
        row += 1
        ttk.Label(frame_general, font="TkDefauktFont 9 bold",
                  text=_("Annotations")).grid(row=row, column=0, padx=4, pady=4,
                                              columnspan=2, sticky="wn")
        # --- --- font
        row += 1
        ttk.Label(frame_general,
                  text=_("Font in annotation popup:")).grid(row=row,
                                                            columnspan=2,
                                                            padx=12, pady=4,
                                                            sticky="wn")
        row += 1
        self.font_popup = FontFrame(frame_general,
                                    CONFIG.get("General", "popup_font"))
        self.font_popup.grid(row=row, columnspan=2, padx=12, pady=4)
        row += 1
        # --- --- author
        frame_author = ttk.Frame(frame_general)
        ttk.Label(frame_author, text=_("Author:")).pack(side='left', padx=4)
        self.author = ttk.Entry(frame_author)
        self.author.insert(0, CONFIG.get("General", "annot_author"))
        self.author.pack(side='left', fill='x', expand=True, padx=4)
        frame_author.grid(row=row, columnspan=2, sticky='ew', padx=8, pady=4)

        #~# --- Update checks
        #~self.confirm_update = ttk.Checkbutton(frame_general,
        #~                                      text=_("Check for updates on start-up"))
        #~self.confirm_update.grid(row=2, column=0, padx=8, pady=4, columnspan=2, sticky='w')
        #~if CONFIG.getboolean('General', 'check_update', fallback=True):
        #~    self.confirm_update.state(('selected', '!alternate'))
        #~else:
        #~    self.confirm_update.state(('!selected', '!alternate'))

        #~# --- Splash supported
        #~self.splash_support = ttk.Checkbutton(frame_general,
        #~                                      text=_("Check this box if the widgets disappear when you click"))
        #~self.splash_support.grid(row=9, column=0, padx=8, pady=4, columnspan=2, sticky='w')
        #~if not CONFIG.getboolean('General', 'splash_supported', fallback=True):
        #~    self.splash_support.state(('selected', '!alternate'))
        #~else:
        #~    self.splash_support.state(('!selected', '!alternate'))

    def _init_annots(self):
        frame_annot = ttk.Frame(self)
        self.notebook.add(frame_annot, text=_('Annotation toolbar'))
        frame_annot.columnconfigure(1, weight=1)
        ttk.Label(frame_annot, text=_("Default appearance"),
                  font="TkDefauktFont 9 bold").grid(row=0, columnspan=2,
                                                    column=0,
                                                    sticky='ew', pady=4)
        self._annot_listbox = tk.Listbox(frame_annot, exportselection=False,
                                         selectmode='browse', justify='right',
                                         width=len(max(self.annot_types, key=len)))
        self._annot_listbox.grid(row=1, column=0)
        self._annot_frames = []
        for annot in self.annot_types:
            self._annot_listbox.insert('end', annot + " ")
            frame = self._init_annot(frame_annot, f"Annot-{annot}")
            frame.grid(row=1, column=1, sticky='ewns', pady=4)
            self._annot_frames.append(frame)
        self._annot_listbox.bind('<<ListboxSelect>>', self._on_listbox_select)
        self._annot_listbox.selection_set(0)
        self._annot_frames[0].lift()

    def _on_listbox_select(self, event):
        try:
            index = self._annot_listbox.curselection()[0]
        except IndexError:
            return
        self._annot_frames[index].lift()

    def _init_annot(self, master, annot_type):
        frame_annot = ttk.Frame(master)
        frame_annot.columnconfigure(1, weight=1)
        row = 0

        # --- fontsize
        if CONFIG.has_option(annot_type, "fontsize"):
            ttk.Label(frame_annot, anchor='e',
                      text=_('Font size:')).grid(row=row, column=0, sticky='ne',
                                                 padx=4, pady=4)
            fontsize = ttk.Entry(frame_annot, validate="key",
                                 validatecommand=(self._is_int, "%P"))
            fontsize.insert(0, CONFIG.get(annot_type, "fontsize"))
            fontsize.grid(row=row, column=1, sticky='ew', padx=4)
            self.annot_props[annot_type]['fontsize'] = fontsize
        # --- align
        if CONFIG.has_option(annot_type, "align"):
            row += 1
            ttk.Label(frame_annot, anchor='e',
                      text=_('Text alignment:')).grid(row=row, column=0, sticky='ne',
                                                      padx=4, pady=4)
            align = AlignSelection(frame_annot, CONFIG.get(annot_type, "align"))
            align.grid(row=row, column=1, sticky='ew', padx=4)
            self.annot_props[annot_type]['align'] = align

        # --- opacity
        row += 1
        ttk.Label(frame_annot, anchor='e',
                  text=_("Opacity:")).grid(row=row, column=0, sticky="e",
                                           padx=4, pady=4)
        opacity_frame = OpacityFrame(frame_annot, CONFIG.getfloat(annot_type, "opacity"))
        opacity_frame.grid(row=row, column=1, sticky='ew')
        self.annot_props[annot_type]['opacity'] = opacity_frame

        # --- linewidth
        if CONFIG.has_option(annot_type, "linewidth"):
            row += 1
            ttk.Label(frame_annot, anchor='e',
                      text=_("Line width:")).grid(row=row, column=0, sticky="e",
                                                  padx=4, pady=4)
            linewidth = ttk.Entry(frame_annot, validate='key',
                                  validatecommand=(self._is_float, '%P'))
            linewidth.insert(0, CONFIG.get(annot_type, "linewidth"))
            linewidth.grid(row=row, column=1, sticky='ew', padx=4)
            self.annot_props[annot_type]['linewidth'] = linewidth
        # --- line endings
        if CONFIG.has_option(annot_type, "ending_left"):
            row += 1
            ttk.Label(frame_annot, anchor='e',
                      text=_('Line start:')).grid(row=row, column=0, sticky='e',
                                                  padx=4, pady=4)
            line_start = LineEndSelection(frame_annot,
                                          CONFIG.getint(annot_type, "ending_left"))
            line_start.grid(row=row, column=1, sticky='ew', padx=4)
            self.annot_props[annot_type]['ending_left'] = line_start
        if CONFIG.has_option(annot_type, "ending_right"):
            row += 1
            ttk.Label(frame_annot, anchor='e',
                      text=_('Line end:')).grid(row=row, column=0, sticky='e',
                                                  padx=4, pady=4)
            line_end = LineEndSelection(frame_annot,
                                        CONFIG.getint(annot_type, "ending_right"))
            line_end.grid(row=row, column=1, sticky='ew', padx=4)
            self.annot_props[annot_type]['ending_right'] = line_end
        # --- color
        row += 1
        color = tuple(int(float(v)*255) for v in CONFIG.get(annot_type, "color").split(","))
        frame_color = ColorFrame(frame_annot, "#%2.2x%2.2x%2.2x" % color)
        ttk.Label(frame_annot, anchor='e',
                  text=_('Color:')).grid(row=row, column=0, sticky='e',
                                         padx=4, pady=4)
        frame_color.grid(row=row, column=1, sticky='ew', padx=4)
        self.annot_props[annot_type]['color'] = frame_color
        # --- fill color
        if CONFIG.has_option(annot_type, "fill"):
            row += 1
            fill = CONFIG.get(annot_type, "fill")
            var_fill = tk.BooleanVar(self, bool(fill))
            if fill:
                fill = "#%2.2x%2.2x%2.2x" % tuple(int(float(v)*255) for v in fill.split(","))
            else:
                fill = "#ffffff"
            frame_fill = OptionalColorFrame(frame_annot, var_fill, fill)
            ttk.Checkbutton(frame_annot, variable=var_fill,
                            command=frame_fill.toggle_state,
                            text=_('Fill:')).grid(row=row, column=0, sticky='e',
                                                  padx=4, pady=4)
            frame_fill.grid(row=row, column=1, sticky='ew', padx=4)
            self.annot_props[annot_type]['fill'] = frame_fill
        # --- icon
        if annot_type == 'Annot-text':
            row += 1
            ttk.Label(frame_annot, anchor='e',
                      text=_('Icon:')).grid(row=row, column=0, sticky='e',
                                            padx=4, pady=4)
            icon = IconSelection(frame_annot, _(CONFIG.get(annot_type, "icon")))
            icon.grid(row=row, column=1, sticky='ew', padx=4)
            self.annot_props[annot_type]['icon'] = icon
        return frame_annot

    def translate(self):
        showinfo(_("Information"),
                 _("The language setting will take effect after restarting the application"),
                 parent=self)

    @staticmethod
    def _validate_float(txt):
        return txt == '' or bool(re.match(r'^\d+\.?\d*$', txt))

    @staticmethod
    def _validate_int(txt):
        return txt == '' or txt.isdigit()

    def ok(self):
        # --- general
        CONFIG.set("General", "language", REV_LANGUAGES[self.lang.get()])
        CONFIG.set("General", "annot_author", self.author.get())
        CONFIG.set("General", "default_zoom", self.zoom.get())
        CONFIG.set("General", "popup_font", self.font_popup.get())
        #~CONFIG.set('General', 'check_update', str(self.confirm_update.instate(('selected',))))
        #~CONFIG.set('General', 'splash_supported', str(not self.splash_support.instate(('selected',))))
        # --- annots
        for annot_type, props in self.annot_props.items():
            for option, widget in props.items():
                CONFIG.set(annot_type, option, str(widget.get()))
        self.destroy()
