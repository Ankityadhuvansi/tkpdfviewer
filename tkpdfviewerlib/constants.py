# -*- coding: utf-8 -*-
"""
tkPDFViewer - Basic PDF viewer
Copyright 2020 Juliette Monsel <j_4321@protonmail.com>

tkPDFViewer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tkPDFViewer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


The images IM_ABOUT, ... are modified versions of icons from the elementary project
(the xfce fork to be precise https://github.com/shimmerproject/elementary-xfce)
Copyright 2007-2013 elementary LLC.


Constants and functions
"""


import os
import configparser
import logging
from logging.handlers import TimedRotatingFileHandler
import warnings
import sys
import gettext
from locale import getdefaultlocale
from subprocess import check_output, CalledProcessError
from tkinter import filedialog
from tkinter import colorchooser

import fitz

APP_NAME = "tkpdfviewer"
REPORT_URL = "https://github.com/j4321/{}/issues".format(APP_NAME)

# --- paths
PATH = os.path.dirname(__file__)

if os.access(PATH, os.W_OK) and os.path.exists(os.path.join(PATH, "images")):
    # the app is not installed
    # local directory containing config files
    PATH_DATA = os.path.join(PATH, 'config')
    if not os.path.exists(PATH_DATA):
        os.mkdir(PATH_DATA)
    PATH_LOCALE = os.path.join(PATH, "locale")
    PATH_IMAGES = os.path.join(PATH, "images")
elif 'linux' in sys.platform:
    # local directory containing config files
    PATH_DATA = os.path.join(os.path.expanduser("~"), ".{}".format(APP_NAME))
    if not os.path.exists(PATH_DATA):
        os.mkdir(PATH_DATA)
    PATH_LOCALE = "/usr/share/locale"
    PATH_IMAGES = "/usr/share/{}/images".format(APP_NAME)
else:
    # local directory containing config files
    PATH_DATA = os.path.join(os.path.expanduser("~"), APP_NAME)
    if not os.path.exists(PATH_DATA):
        os.mkdir(PATH_DATA)
    PATH_LOCALE = os.path.join(PATH, "locale")
    PATH_IMAGES = os.path.join(PATH, "images")

PATH_CONFIG = os.path.join(PATH_DATA, "{}.ini".format(APP_NAME))
PATH_LOG = os.path.join(PATH_DATA, "{}.log".format(APP_NAME))
PIDFILE = os.path.join(PATH_DATA, "{}.pid".format(APP_NAME))


# --- images
IMAGES = {}
for img in os.listdir(PATH_IMAGES):
    name, ext = os.path.splitext(img)
    if ext == '.png':
        IMAGES[name] = os.path.join(PATH_IMAGES, img)
ICONS = ['warning', 'information', 'question', 'error']
IM_SCROLL_ALPHA = os.path.join(PATH_IMAGES, 'scroll.png')

# --- log
handler = TimedRotatingFileHandler(PATH_LOG, when='midnight',
                                   interval=1, backupCount=7)
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)-15s %(levelname)s: %(message)s',
                    handlers=[handler])
logging.getLogger().addHandler(logging.StreamHandler())


def customwarn(message, category, filename, lineno, file=None, line=None):
    """Redirect warnings to log"""
    logging.warn(warnings.formatwarning(message, category, filename, lineno))


warnings.showwarning = customwarn


# --- config
CONFIG = configparser.RawConfigParser()

default_config = {
    'General': {
        "annot_author": os.getlogin(),
        "popup_font": "DejaVu\ Sans 10",
        "default_zoom": "width",
        "recent_files": "",
        "check_update": "True",
        "splash_supported": str(os.environ.get('DESKTOP_SESSION') != 'plasma'),
    },
    'Annot-text': {
        "opacity": "1.0",
        "color": "1.0,1.0,0.0",
        "icon": "Note",
    },
    'Annot-caret': {
        "opacity": "1.0",
        "color": "1.0,0.0,0.0",
    },
    'Annot-freetext': {
        "opacity": "1.0",
        "color": "1.0,0.0,0.0",
        "fill": "",
        "fontsize": "10",
        "align": str(fitz.TEXT_ALIGN_LEFT),
        "linewidth": "0.0",
    },
    'Annot-rectangle': {
        "opacity": "1.0",
        "color": "1.0,0.0,0.0",
        "fill": "",
        "linewidth": "2.0",
    },
    'Annot-circle': {
        "opacity": "1.0",
        "color": "0.0,1.0,0.0",
        "fill": "",
        "linewidth": "1.0",
    },
    'Annot-line': {
        "opacity": "1.0",
        "color": "0.0,0.0,1.0",
        "linewidth": "1.0",
        "ending_left": "0",
        "ending_right": "0",
    },
    'Annot-arrow': {
        "opacity": "1.0",
        "color": "0.0,0.0,0.0",
        "linewidth": "2.0",
        "ending_left": "0",
        "ending_right": str(fitz.PDF_ANNOT_LE_CLOSED_ARROW),
    },
    'Annot-polygon': {
        "opacity": "1.0",
        "color": "1.0,0.0,0.0",
        "fill": "",
        "linewidth": "1.0",
    },
    'Annot-polyline': {
        "opacity": "1.0",
        "color": "1.0,0.0,0.0",
        "linewidth": "1.0",
    },
    'Annot-ink': {
        "opacity": "1.0",
        "color": "0.0,0.0,1.0",
        "linewidth": "2.0",
    },
}


for section, opts in default_config.items():
    CONFIG.setdefault(section, opts)
CONFIG.read(PATH_CONFIG)


def save_config():
    with open(PATH_CONFIG, 'w') as file:
        CONFIG.write(file)


# --- language
LANGUAGE = CONFIG.get('General', 'language', fallback='')

LANGUAGES = {"fr": "Français", "en": "English"}
REV_LANGUAGES = {val: key for key, val in LANGUAGES.items()}

if LANGUAGE not in LANGUAGES:
    # Check the default locale
    LANGUAGE = getdefaultlocale()[0].split('_')[0]
    if LANGUAGE in LANGUAGES:
        CONFIG.set("General", "language", LANGUAGE)
    else:
        CONFIG.set("General", "language", "en")
    save_config()

gettext.bind_textdomain_codeset(APP_NAME, "UTF-8")
gettext.bindtextdomain(APP_NAME, PATH_LOCALE)
gettext.textdomain(APP_NAME)

gettext.translation(APP_NAME, PATH_LOCALE,
                    languages=[LANGUAGE],
                    fallback=True).install()

# --- alternative filebrowser / colorchooser
ZENITY = False

try:
    import tkfilebrowser as tkfb
except ImportError:
    tkfb = False

try:
    import tkcolorpicker as tkcp
except ImportError:
    tkcp = False

if os.name != "nt":
    paths = os.environ['PATH'].split(":")
    for path in paths:
        if os.path.exists(os.path.join(path, "zenity")):
            ZENITY = True


def askopenfilename(defaultextension, filetypes, initialdir, initialfile="", title=_('Open'), **options):
    """
    Plateform specific file browser.

    Arguments:
        - defaultextension: extension added if none is given
        - initialdir: directory where the filebrowser is opened
        - filetypes: [('NOM', '*.ext'), ...]
    """
    if tkfb:
        return tkfb.askopenfilename(title=title,
                                    defaultext=defaultextension,
                                    filetypes=filetypes,
                                    initialdir=initialdir,
                                    initialfile=initialfile,
                                    **options)
    elif ZENITY:
        try:
            args = ["zenity", "--file-selection",
                    "--filename", os.path.join(initialdir, initialfile)]
            for ext in filetypes:
                args += ["--file-filter", "%s|%s" % ext]
            args += ["--title", title]
            file = check_output(args).decode("utf-8").strip()
            filename, ext = os.path.splitext(file)
            if not ext:
                ext = defaultextension
            return filename + ext
        except CalledProcessError:
            return ""
        except Exception:
            return filedialog.askopenfilename(title=title,
                                              defaultextension=defaultextension,
                                              filetypes=filetypes,
                                              initialdir=initialdir,
                                              initialfile=initialfile,
                                              **options)
    else:
        return filedialog.askopenfilename(title=title,
                                          defaultextension=defaultextension,
                                          filetypes=filetypes,
                                          initialdir=initialdir,
                                          initialfile=initialfile,
                                          **options)


def asksaveasfilename(defaultextension, filetypes, initialdir=".", initialfile="", title=_('Save As'), **options):
    """
    Plateform specific file browser for saving a file.

    Arguments:
        - defaultextension: extension added if none is given
        - initialdir: directory where the filebrowser is opened
        - filetypes: [('NOM', '*.ext'), ...]
    """
    if tkfb:
        return tkfb.asksaveasfilename(title=title,
                                      defaultext=defaultextension,
                                      initialdir=initialdir,
                                      filetypes=filetypes,
                                      initialfile=initialfile,
                                      **options)
    elif ZENITY:
        try:
            args = ["zenity", "--file-selection",
                    "--filename", os.path.join(initialdir, initialfile),
                    "--save", "--confirm-overwrite"]
            for ext in filetypes:
                args += ["--file-filter", "%s|%s" % ext]
            args += ["--title", title]
            file = check_output(args).decode("utf-8").strip()
            if file:
                filename, ext = os.path.splitext(file)
                if not ext:
                    ext = defaultextension
                return filename + ext
            else:
                return ""
        except CalledProcessError:
            return ""
        except Exception:
            return filedialog.asksaveasfilename(title=title,
                                                defaultextension=defaultextension,
                                                initialdir=initialdir,
                                                filetypes=filetypes,
                                                initialfile=initialfile,
                                                **options)
    else:
        return filedialog.asksaveasfilename(title=title,
                                            defaultextension=defaultextension,
                                            initialdir=initialdir,
                                            filetypes=filetypes,
                                            initialfile=initialfile,
                                            **options)


def askcolor(color=None, **options):
    """
    Plateform specific color chooser.

    return the chose color in #rrggbb format.
    """
    if tkcp:
        color = tkcp.askcolor(color, **options)
        if color:
            return color[1]
        else:
            return None
    elif ZENITY:
        try:
            args = ["zenity", "--color-selection", "--show-palette"]
            if "title" in options:
                args += ["--title", options["title"]]
            if color:
                args += ["--color", color]
            color = check_output(args).decode("utf-8").strip()
            if color:
                if color[0] == "#":
                    if len(color) == 13:
                        color = "#%s%s%s" % (color[1:3], color[5:7], color[9:11])
                elif color[:4] == "rgba":
                    color = color[5:-1].split(",")
                    color = '#%02x%02x%02x' % (int(color[0]), int(color[1]), int(color[2]))
                elif color[:3] == "rgb":
                    color = color[4:-1].split(",")
                    color = '#%02x%02x%02x' % (int(color[0]), int(color[1]), int(color[2]))
                else:
                    raise TypeError("Color formatting not understood.")
            return color
        except CalledProcessError:
            return None
        except Exception:
            color = colorchooser.askcolor(color, **options)
            return color[1]
    else:
        color = colorchooser.askcolor(color, **options)
        return color[1]


# --- misc
# other functions
