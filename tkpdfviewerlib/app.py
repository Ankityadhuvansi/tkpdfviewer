# -*- coding: utf-8 -*-
"""
tkPDFViewer - Basic PDF viewer
Copyright 2020 Juliette Monsel <j_4321@protonmail.com>

tkPDFViewer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tkPDFViewer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Main window
"""
# TODO - issue with opacity of annotations
# TODO - annotation: reshape annots (X), undo/redo, respect flags hidden, locked, ...
# TODO - spell check in text
# TODO - structure: separate class for viewer canvas to reduce App length
# TODO - quick color change from toolbar? (reorganize toolbar with drop-down shape selection + linewidth + color??)
# TODO - text selection, then annots like highlighting, underline, ...


import tkinter as tk
from tkinter import ttk
import os
import re
import logging
import traceback
from webbrowser import open as url_open

import fitz
from PIL import Image, ImageTk

from . import constants as cst
from .autoscrollbar import AutoScrollbar
from .constants import IMAGES, askopenfilename, asksaveasfilename
from .about import About
from .config import Config
from .messagebox import showinfo, showerror, askpassword, askyesnocancel, askokcancel, askoptions
from .popup import Popup, AnnotProps
from .toggle import ToggleVar
from .tooltip import TooltipCanvasWrapper, TooltipWrapper, PreviewCanvasWrapper


class App(tk.Tk):

    annot_draggable = [
        fitz.PDF_ANNOT_CARET,
        fitz.PDF_ANNOT_CIRCLE,
        fitz.PDF_ANNOT_FREE_TEXT,
        fitz.PDF_ANNOT_INK,
        fitz.PDF_ANNOT_LINE,
        fitz.PDF_ANNOT_POLY_LINE,
        fitz.PDF_ANNOT_POLYGON,
        fitz.PDF_ANNOT_SQUARE,
        fitz.PDF_ANNOT_TEXT,
    ]

    annot_resizable_rect = [
        fitz.PDF_ANNOT_CIRCLE,
        fitz.PDF_ANNOT_FREE_TEXT,
        fitz.PDF_ANNOT_SQUARE,
    ]

    def __init__(self, filepath=None):
        tk.Tk.__init__(self, className=cst.APP_NAME)
        logging.info('Starting {}'.format(cst.APP_NAME))

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(3, weight=1)

        # pt to px conv
        frame = tk.Frame(width='100p')
        self._zoom_pt_to_px = frame.winfo_reqwidth() / 10000  # the extra 1/100 is to convert % to fraction
        frame.destroy()

        self.protocol('WM_DELETE_WINDOW', self.quit)
        self._zoom_delay = ''
        # --- pdf file
        self._title = cst.APP_NAME
        self.filepath = ''
        self._mtime = 0  # last modif time from tkPDFviewer
        self.doc = None
        self.pages = []
        self._unsaved_changes = False  # True if unsaved changes
        # thumbnails
        self._thumbs = []
        # annots
        self._annots = {}
        self._annot_popups = {}
        # links
        self._links = {}
        self._current_page = tk.IntVar(self, 1)
        self._page_bbox = ()
        self._page = ImageTk.PhotoImage(Image.new("RGB", (1, 1), 'white'),
                                        master=self)

        # --- images
        self._images = {name: tk.PhotoImage(f'img_{name}', file=IMAGES[name],
                                            master=self)
                        for name, path in IMAGES.items()}

        self.iconphoto(True, 'img_icon')

        # --- style
        style = ttk.Style(self)
        style.theme_use('clam')
        style.configure('tooltip.TLabel', background='black',
                        foreground='white')
        style.configure('tooltip.TFrame', background='black')
        style.configure('white.TLabel', background='white', foreground="black")
        style.configure('Treeview', indent=10)
        style.configure('small.TMenubutton', arrowsize=3, arrowpadding=1)
        style.map('TMenubutton', arrowcolor=[('disabled', style.lookup('TButton', 'foreground', ['disabled']))])

        style.layout('toggle.TCheckbutton', style.layout('TButton'))
        style.layout('toggle.TRadiobutton', style.layout('TButton'))
        bmap = style.map('TButton')
        bmap['relief'] = [('selected', 'sunken'), ('!selected', 'raised')]
        bmap['background'].extend(
            [('selected', style.lookup('TButton', 'background', ['pressed'])),
             ('!selected', style.lookup('TButton', 'background'))]
        )
        bmap['focuscolor'] = [('selected', style.lookup('TButton', 'background', ['pressed'])),
                              ('!selected', style.lookup('TButton', 'background'))]
        #~bmap['background'] = [('selected', '#bab5ab')]
        style.configure('toggle.TCheckbutton', focusthickness=0)
        style.configure('toggle.TRadiobutton', focusthickness=0)
        style.map('toggle.TCheckbutton', **bmap)
        style.map('toggle.TRadiobutton', **bmap)
        style.map('TCombobox',
                  foreground=[],
                  fieldbackground=[('readonly', style.lookup('TCombobox', 'background'))])
        style.layout('thumb.TRadiobutton',
                     [('Radiobutton.padding',
                      {'sticky': 'nswe',
                       'children': [('Radiobutton.label', {'sticky': 'nswe'})]})])
        style.configure('thumb.TRadiobutton', background='white')
        sel_bg = style.lookup('TEntry', 'selectbackground', ('focus',))

        style.map('thumb.TRadiobutton', background=[('selected', sel_bg)])
        bg = style.lookup('TFrame', 'background', default='#ececec')
        self.configure(bg=bg)
        self.option_add('*Button.background', bg)
        self.option_add('*Button.highlightThickness', 0)
        self.option_add('*Button.relief', 'flat')
        self.option_add('*Toplevel.background', bg)
        self.option_add('*Text.relief', 'flat')
        self.option_add('*Text.exportselection', False)
        self.option_add('*Text.highlightThickness', 0)
        self.option_add('*Text.borderWidth', 0)
        self.option_add('*Canvas.background', bg)
        self.option_add('*Menu.background', bg)
        self.option_add('*Menu.tearOff', False)
        self.option_add('*Toplevel.background', bg)

        # tooltips
        self.tooltips = TooltipWrapper(self)

        #~# --- menu
        #~menu = tk.Menu(self)

        #~# --- --- file
        #~menu_file = tk.Menu(menu)
        #~menu_file.add_command(label=_('Open'), command=self.open)

        #~# --- --- help
        #~menu_help = tk.Menu(menu)
        #~menu_help.add_command(label=_('About'), image='img_about',
        #~                      compound='left', command=self.about)

        #~menu.add_cascade(label=_('File'), menu=menu_file)
        #~menu.add_cascade(label=_('Language'), menu=menu_lang)
        #~menu.add_cascade(label=_('Help'), menu=menu_help)

        #~self.configure(menu=menu)

        # --- content
        toolbar = ttk.Frame(self, padding=4)
        self.search_bar = ttk.Frame(self, relief='sunken', border=1, padding=2)
        self.panes = tk.PanedWindow(self, orient='horizontal')

        toolbar.grid(row=0, sticky='ew')
        ttk.Separator(self, orient='horizontal').grid(row=1, sticky='ew')
        self.search_bar.grid(row=2, sticky='ew')
        self.search_bar.grid_remove()
        self.panes.grid(row=3, sticky='ewns')

        # --- --- toolbar
        self._toolbar_btns = []  # to be able to toggle buttons state
        toggle_leftpane = ttk.Checkbutton(toolbar, padding=1,
                                          style='toggle.TCheckbutton',
                                          image='img_leftpane',
                                          command=self.toggle_leftpane)
        toggle_leftpane.pack(side='left', padx=0)
        toggle_leftpane.state(['!alternate', 'selected'])
        self.tooltips.add_tooltip(toggle_leftpane, _("Toggle side pane visibility"))

        # --- --- --- open - save
        ttk.Separator(toolbar, orient='vertical').pack(side='left', padx=8, fill='y')
        open_btn = ttk.Button(toolbar, padding=0, image='img_open',
                              command=self.open)
        open_btn.pack(side='left')
        self.tooltips.add_tooltip(open_btn, _("Open"))
        # recent files
        self.menu_recent_files = tk.Menu(self)
        recent_files = cst.CONFIG.get('General', 'recent_files', fallback='').split(', ')
        self.recent_files = [f for f in recent_files if f and os.path.exists(f)]
        for f in self.recent_files:
            self.menu_recent_files.add_command(label=f,
                                               command=lambda file=f: self.open(filename=file))
        self._recent_btn = ttk.Menubutton(toolbar, width=0, padding=0,
                                          style='small.TMenubutton',
                                          menu=self.menu_recent_files)
        self._recent_btn.pack(side='left', fill='y', padx=(0, 4))
        if not self.recent_files:
            self._recent_btn.state(["disabled"])
        self.tooltips.add_tooltip(self._recent_btn, _("Open recent file"))


        self._save_btn = ttk.Button(toolbar, padding=0, image='img_save',
                                    command=self.save)
        self._save_btn.pack(side='left', padx=4)
        self._save_btn.state(['disabled'])
        self.tooltips.add_tooltip(self._save_btn, _("Save"))
        saveas_btn = ttk.Button(toolbar, padding=0, image='img_saveas',
                                command=self.saveas)
        saveas_btn.pack(side='left')
        self._toolbar_btns.append(saveas_btn)
        self.tooltips.add_tooltip(saveas_btn, _("Save as"))
        reload_btn = ttk.Button(toolbar, padding=0, image='img_refresh',
                                command=self.reload)
        reload_btn.pack(side='left', padx=(4, 0))
        self._toolbar_btns.append(reload_btn)
        self.tooltips.add_tooltip(reload_btn, _("Reload document"))

        # --- --- --- navigate
        ttk.Separator(toolbar, orient='vertical').pack(side='left', padx=8, fill='y')
        self.page_entry = ttk.Entry(toolbar, width=2, justify='right')
        self.page_entry.pack(side='left', fill='y')
        self.page_entry.bind('<Return>', self._set_page)
        self._toolbar_btns.append(self.page_entry)
        self.tooltips.add_tooltip(self.page_entry, _("Set page"))
        self.page_total = ttk.Label(toolbar, text='/1')
        self.page_total.pack(side='left', fill='y')
        prev_btn = ttk.Button(toolbar, padding=0, image='img_up',
                              command=self.prev_page)
        prev_btn.pack(side='left', padx=4)
        self._toolbar_btns.append(prev_btn)
        self.tooltips.add_tooltip(prev_btn, _("Previous page"))
        next_btn = ttk.Button(toolbar, padding=0, image='img_down',
                              command=self.next_page)
        next_btn.pack(side='left')
        self._toolbar_btns.append(next_btn)
        self.tooltips.add_tooltip(next_btn, _("Next page"))

        # --- --- --- annots
        ttk.Separator(toolbar, orient='vertical').pack(side='left', padx=8, fill='y')
        self._annotate = ToggleVar(self, deselectcmd=self._annot_deselect)  # kind of annotation
        annot_tooltips = {
            'text': _("Add text annotation"),
            'freetext': _("Add freetext annotation"),
            'rectangle': _("Add rectangle annotation"),
            'circle': _("Add circle annotation"),
            'polygon': _("Add polygon annotation"),
            'line': _("Add line annotation"),
            'arrow': _("Add arrow annotation"),
            'polyline': _("Add polyline annotation"),
            'ink': _("Add free hand annotation"),
            'caret': _("Add caret annotation"),
        }
        shape = {'outline': 'grey', 'width': 2, 'fill': ''}
        line = {'fill': 'grey', 'width': 2}
        arrow = {'fill': 'grey', 'width': 2, 'arrow': 'last',
                 'arrowshape': (16, 20, 6)}
        self._annot_drawing = {  # drawing function associated with each annot type
            'freetext': lambda *args: self.viewer.create_rectangle(*args, dash=(3,), **shape),
            'rectangle': lambda *args: self.viewer.create_rectangle(*args, **shape),
            'circle': lambda *args: self.viewer.create_oval(*args, **shape),
            'polygon': lambda *args: self.viewer.create_polygon(*args, **shape),
            'line': lambda *args: self.viewer.create_line(*args, **line),
            'arrow': lambda *args: self.viewer.create_line(*args, **arrow),
            'polyline': lambda *args: self.viewer.create_line(*args, **line),
            'ink': lambda *args: self.viewer.create_line(*args, **line),
        }
        self._annot_cursors = {
            'text': "plus",
            'freetext': "cross",
            'rectangle': "cross",
            'circle': "cross",
            'polygon': "pencil",
            'line': "pencil",
            'arrow': "pencil",
            'polyline': "pencil",
            'ink': "pencil",
            'caret': "xterm",
        }

        for annot, tooltip in annot_tooltips.items():
            btn = ttk.Radiobutton(toolbar, padding=1, image=f'img_annot-{annot}',
                                  style='toggle.TRadiobutton',
                                  value=annot,
                                  variable=self._annotate,
                                  command=self.toggle_add_annot)
            btn.state(['!alternate'])
            btn.pack(side='left', padx=(0, 4))
            self._toolbar_btns.append(btn)
            self.tooltips.add_tooltip(btn, tooltip)
        self._draw = 0, [] # iid, coords
        self._annot_drag_pos_init = ()  # mouse coords when dragging started
        self._annot_drag_pos = ()       # mouse coords when dragging annot


        # --- --- --- settings
        settings_btn = ttk.Button(toolbar, padding=0, image='img_settings',
                                  command=self.settings)
        settings_btn.pack(side='right')
        self.tooltips.add_tooltip(settings_btn, _("Settings"))

        # --- --- --- info
        ttk.Separator(toolbar, orient='vertical').pack(side='right', padx=8, fill='y')
        about_btn = ttk.Button(toolbar, padding=0, image='img_about',
                               command=lambda: About(self))
        about_btn.pack(side='right')
        self.tooltips.add_tooltip(about_btn,
                                  _("About {app_name}").format(app_name=cst.APP_NAME))
        info_btn = ttk.Button(toolbar, padding=0, image='img_info',
                              command=self.show_info)
        info_btn.pack(side='right', padx=4)
        self._toolbar_btns.append(info_btn)
        self.tooltips.add_tooltip(info_btn, _("Document properties"))

        # --- --- --- search
        ttk.Separator(toolbar, orient='vertical').pack(side='right', padx=4, fill='y')
        find_btn = ttk.Checkbutton(toolbar, padding=1, image='img_find',
                                   style='toggle.TCheckbutton',
                                   command=self.search_bar_toggle)
        find_btn.state(['!alternate'])
        find_btn.pack(side='right', padx=4)
        self._toolbar_btns.append(find_btn)
        self.tooltips.add_tooltip(find_btn, _("Search in document"))


        # --- --- --- zoom
        default_zoom = cst.CONFIG.get("General", "default_zoom")
        if default_zoom in ["width", "page"]:
            fit_zoom = default_zoom
            init_zoom = 100
        else:
            fit_zoom = ""
            try:
                init_zoom = float(default_zoom)
            except ValueError:
                init_zoom = 100
        self._fit_zoom = tk.StringVar(self, fit_zoom)
        self.zoom = tk.DoubleVar(self, init_zoom)
        self.zoom_entry = ttk.Entry(toolbar, width=8, justify='center')
        self.zoom_entry.bind('<Return>', self._validate_zoom)
        self.zoom_entry.bind('<FocusOut>', self._reset_zoom)
        self.tooltips.add_tooltip(self.zoom_entry, _("Set zoom level"))
        zoom_menu = tk.Menu(toolbar)
        zoom_menu.add_radiobutton(label=_('Fit width'), value='width',
                                  variable=self._fit_zoom,
                                  command=self.view_page)
        zoom_menu.add_radiobutton(label=_('Fit page'), value='page',
                                  variable=self._fit_zoom,
                                  command=self.view_page)
        zoom_menu.add_separator()
        for zoom in [50, 75, 100, 125, 150, 200, 300, 400]:
            zoom_menu.add_command(label=f'{zoom}%',
                                  command=lambda z=zoom: self._set_zoom(z))
        self.zoom.trace_add('write', self._zoom_trace)
        self.zoom.set(init_zoom)

        zoom_btn = ttk.Menubutton(toolbar, width=0, padding=0,
                                  style='small.TMenubutton', menu=zoom_menu)
        zoom_btn.pack(side='right', fill='y', padx=(0, 4))
        self._toolbar_btns.append(zoom_btn)
        self._toolbar_btns.append(self.zoom_entry)
        self.tooltips.add_tooltip(zoom_btn, _("Set zoom level"))
        self.zoom_entry.pack(side='right', fill='y')

        self._set_toolbar_state("disabled")

        # --- --- search bar
        self._search_string = ''
        self._search_index = 0
        self._search_results_list = []  # [(pagenb, index in page), ... ]
        self._search_results = {}
        self.search_bar.grid_columnconfigure(0, weight=1)
        self.search_bar.grid_columnconfigure(2, weight=1)
        self.search_entry = ttk.Entry(self.search_bar)
        self.search_entry.bind('<Return>', self._search_validate)
        self.search_entry.grid(row=0, column=0, sticky='ens')
        sprev_btn = ttk.Button(self.search_bar, image='img_up', padding=0,
                               command=self.search_prev)
        sprev_btn.grid(row=0, column=1)
        snext_btn = ttk.Button(self.search_bar, image='img_down', padding=0,
                               command=self.search_next)
        snext_btn.grid(row=0, column=2, sticky='w')
        self.tooltips.add_tooltip(sprev_btn, _("Previous search result"))
        self.tooltips.add_tooltip(snext_btn, _("Next search result"))
        tk.Button(self.search_bar, image='img_close',
                   command=self.search_bar_toggle).grid(row=0, column=3, sticky='ns')


        # --- --- left pane
        self.left_pane = ttk.Notebook(self.panes)
        self.panes.add(self.left_pane, minsize=130, width=130)

        # --- --- --- Thumbnails
        frame_thumbnails = ttk.Frame(self.left_pane)
        frame_thumbnails.grid_rowconfigure(0, weight=1)
        frame_thumbnails.grid_columnconfigure(0, weight=1)
        self.thumbnails = tk.Text(frame_thumbnails, wrap='word', cursor='arrow')
        thumb_scroll = ttk.Scrollbar(frame_thumbnails, orient='vertical',
                                     command=self.thumbnails.yview)
        self.thumbnails.configure(yscrollcommand=thumb_scroll.set)

        self.thumbnails.grid(row=0, column=0, sticky='ewsn')
        thumb_scroll.grid(row=0, column=1, sticky='sn')

        self.left_pane.add(frame_thumbnails, image='img_thumbnails')

        # --- --- --- ToC
        frame_toc = ttk.Frame(self.left_pane)
        frame_toc.grid_columnconfigure(0, weight=1)
        frame_toc.grid_rowconfigure(0, weight=1)
        self.toc = ttk.Treeview(frame_toc, show='tree', column=['page'])
        scrolltoc = AutoScrollbar(frame_toc, orient='vertical',
                                  command=self.toc.yview)
        self.toc.configure(yscrollcommand=scrolltoc.set)
        self.toc.column('#0', stretch=True, width=90)
        self.toc.column('page', stretch=False, width=32, anchor='e')

        self.toc.grid(row=0, column=0, sticky='ewns')
        scrolltoc.grid(row=0, column=1, sticky='ns')
        self.left_pane.add(frame_toc, image='img_outline')

        # --- --- --- Annots
        frame_annots = ttk.Frame(self.left_pane)
        frame_annots.grid_columnconfigure(0, weight=1)
        frame_annots.grid_rowconfigure(0, weight=1)

        self._annot_menu = tk.Menu(self, tearoff=False)  # right click menu
        self._annot_menu.add_command(label=_('Delete'))
        self._annot_menu.add_command(label=_('Properties'))

        self.annots = ttk.Treeview(frame_annots, show='tree',
                                   columns=['color', 'author', 'text'],
                                   displaycolumns=[])
        scrollannot = AutoScrollbar(frame_annots, orient='vertical',
                                    command=self.annots.yview)
        self.annots.configure(yscrollcommand=scrollannot.set)
        self.annots.grid(row=0, column=0, sticky='ewns')
        scrollannot.grid(row=0, column=1, sticky='ns')
        self.left_pane.add(frame_annots, image='img_annot')

        #~# --- --- --- Bookmarks
        #~frame_bookmarks = ttk.Frame(self.left_pane)
        #~self.left_pane.add(frame_bookmarks, image='img_bookmark')

        # --- --- viewer
        viewer_frame = ttk.Frame(self.panes)

        viewer_frame.grid_columnconfigure(0, weight=1)
        viewer_frame.grid_rowconfigure(0, weight=1)
        self.viewer = tk.Canvas(viewer_frame, width=794, height=1123)
        self.viewer.create_rectangle(0, 0, 794, 1123, outline='', fill=self.viewer['bg'],
                                     tags='bg')
        self.annot_tooltips = TooltipCanvasWrapper(self.viewer)
        self.preview_tooltips = PreviewCanvasWrapper(self.viewer,
                                                     self._links_render_preview)

        scrollx = AutoScrollbar(viewer_frame, orient='horizontal',
                                command=self.viewer.xview)
        scrolly = AutoScrollbar(viewer_frame, orient='vertical',
                                command=self.viewer.yview)
        self.viewer.configure(xscrollcommand=scrollx.set,
                              yscrollcommand=scrolly.set)
        self.viewer.grid(row=0, column=0, sticky='ewns')
        scrollx.grid(row=1, column=0, sticky='we')
        scrolly.grid(row=0, column=1, sticky='ns')
        self.viewer.create_image(0, 0, anchor='nw', tags='page')
        self.viewer.bind('<Configure>', self.resize_page)

        self._unlock_button = ttk.Button(viewer_frame, text=_('Unlock'),
                                         compound='left', image='img_lock',
                                         command=self.unlock_doc)
        self.panes.add(viewer_frame)

        # --- file monitoring for external changes
        self._monitoring_id = None

        # --- bindings
        # scroll
        self.viewer.bind('<4>', self.scroll_up)
        self.viewer.bind('<5>', self.scroll_down)
        self.viewer.bind('<Shift-4>', self.scroll_left)
        self.viewer.bind('<Shift-5>', self.scroll_right)
        # zoom
        self.viewer.bind('<Control-5>', self.zoom_out)
        self.viewer.bind('<Control-4>', self.zoom_in)
        self.bind('<minus>', self.zoom_out)
        self.bind('<plus>', self.zoom_in)
        # links and annots hover cursor
        self._page_cursor = ''
        self.viewer.tag_bind('bg', '<Enter>',
                             lambda ev: self.viewer.configure(cursor=""))
        self.viewer.tag_bind('bg', '<Leave>',
                             lambda ev: self.viewer.configure(cursor=self._page_cursor))
        self.viewer.tag_bind('annot', '<Enter>',
                             lambda ev: self.viewer.configure(cursor="hand1"))
        self.viewer.tag_bind('annot', '<Leave>',
                             lambda ev: self.viewer.configure(cursor=self._page_cursor))
        self.viewer.tag_bind('link', '<Enter>', lambda ev:
                             self.viewer.configure(cursor="hand1"))
        self.viewer.tag_bind('link', '<Leave>',
                             lambda ev: self.viewer.configure(cursor=self._page_cursor))
        # annot dragging
        self.viewer.tag_bind('draggable', '<ButtonPress-1>', self._annot_start_drag)
        self.viewer.tag_bind('draggable', '<B1-Motion>', self._annot_drag)
        # navigate
        self.bind('<Left>', self.prev_page)
        self.bind('<Right>', self.next_page)
        self.bind('<Up>', self.prev_page)
        self.bind('<Down>', self.next_page)
        self.bind('<space>', self.next_page)
        self.bind('<Return>', self.next_page)
        # keyboard shortcuts
        self.bind('<Control-o>', self.open)
        self.bind('<Control-r>', self.reload)
        self.bind('<Control-s>', self.save)
        self.bind('<Control-Shift-S>', self.saveas)
        self.bind('<Control-f>', self.search_bar_toggle)
        # class bindings
        self.bind_class('TCombobox', '<<ComboboxSelected>>',
                        lambda ev: ev.widget.selection_clear(),
                        add=True)
        self.bind_class('TEntry', '<Control-a>',
                        lambda ev: ev.widget.selection_range(0, 'end'))
        self.bind_class('Text', '<Control-a>',
                        lambda ev: ev.widget.tag_add('sel', '1.0', 'end'))
        self.bind_class('Text', '<Control-z>', self._text_undo)
        self.bind_class('Text', '<Control-y>', self._text_redo)

        # --- load doc
        if filepath:
            self.open_doc(filepath)

    @property
    def current_page(self):
        return self._current_page.get()

    @current_page.setter
    def current_page(self, value):
        self._current_page.set(value)

    @property
    def unsaved_changes(self):
        return self._unsaved_changes

    @unsaved_changes.setter
    def unsaved_changes(self, value):
        if not self.filepath:
            self._unsaved_changes = False
            #~self.title(cst.APP_NAME)
            return
        self._unsaved_changes = value
        self.title(self._title + '*' * value)
        if self.doc.can_save_incrementally():
            self._save_btn.state(['!'*value + 'disabled'])

    # --- class bindings
    @staticmethod
    def _text_undo(event):
        try:
            event.widget.edit_undo()
        except tk.TclError:
            pass  # nothing to undo
        return "break"

    @staticmethod
    def _text_redo(event):
        try:
            event.widget.edit_redo()
        except tk.TclError:
            pass   # nothing to redo
        return "break"

    # --- modif watching
    def _file_modified(self):
        """Called when file is modified externally."""
        logging.warning("File '%s' was modified externally.", self.filepath)
        if os.stat(self.filepath).st_mtime > self._mtime:
            ans = _('Reload')  # reload if no unsaved change
            if self.unsaved_changes:
                ans = askoptions('Warning',
                                 _('The document has been modified outside tkPDFViewer. What do you want to do?'),
                                 self, 'warning', _('Reload'), _('Overwrite'), _('Cancel'))
            if ans == _("Reload"):
                self.reload()
            elif ans == _("Overwrite"):
                self.save()

    def _file_deleted(self):
        """Called when file is deleted or moved."""
        logging.warning("File '%s' no longer exists.", self.filepath)
        self.unsaved_changes = True

    def _file_monitoring_stop(self):
        """Stop monitoring file for external changes."""
        try:
            self.after_cancel(self._monitoring_id)
        except ValueError:
            pass

    def _file_monitoring_start(self):
        """Start monitoring file for external changes."""
        self._file_monitoring_stop()
        self._monitoring_id = self.after(300, self._file_monitoring)

    def _file_monitoring(self):
        """Monitor file for external changes."""
        try:
            if self._mtime < os.stat(self.filepath).st_mtime:
                self._file_modified()
                return
        except FileNotFoundError:
            self._file_deleted()
            return
        self._monitoring_id = self.after(300, self._file_monitoring)

    # --- search
    def _search_reset(self, string=''):
        self._search_string = string
        self._search_index = 0
        self._search_results_list.clear()
        for p in self._search_results:
            self._search_results[p].clear()
        self.viewer.delete('search')

    def search_bar_toggle(self, event=None):
        if self.search_bar.winfo_ismapped():
            self.search_bar.grid_remove()
            self._search_reset()
            self.search_entry.delete(0, 'end')
        else:
            self.search_bar.grid()
            self.search_entry.focus_set()

    def _search(self, string):
        self._search_reset(string)
        # populate self._search_results and self._search_results_list
        for p, page in enumerate(self.pages, 1):
            self._search_results[p].extend(page.searchFor(string))
            self._search_results[p].sort(key=lambda rect: (rect[1], rect[0]))
            self._search_results_list.extend([(p, i) for i in range(len(self._search_results[p]))])

    def _display_prev(self):
        if not self._search_results_list:
            showinfo(_("Search"), _("No result found."))
        else:
            try:
                self._search_index = self._search_results_list.index((self.current_page, 0))
            except ValueError:
                self._search_results_list.append((self.current_page, 0))
                self._search_results_list.sort()
                self._search_index = self._search_results_list.index((self.current_page, 0))
                del self._search_results_list[self._search_index]
                if self._search_index > 0:
                    self._search_index -= 1
                self.view_page(self._search_results_list[self._search_index][0])
            else:
                self._render_search_results()
            self._see_current_search_result()

    def search_prev(self):
        string = self.search_entry.get()
        if self._search_string == string:
            if self._search_index == 0:
                showinfo(_("Search"), _("No more results."))
            else:
                self._search_index -= 1
            self._display_current_search_result()
        else:
            self._search(string)
            self._display_prev()

    def _search_validate(self, event):
        string = self.search_entry.get()
        if self._search_string == string:
            if not self._search_results_list:
                showinfo(_("Search"), _("No result found."))
            elif self._search_results_list[self._search_index][0] != self.current_page:
                self._display_next()
            else:
                self._search_index += 1
                self._display_current_search_result()
        else:
            self._search(string)
            self._display_next()
        return "break"

    def _see_current_search_result(self):
        """Make sure current search results is visible."""
        bbox = self.viewer.bbox('search_current')
        if bbox:
            w, h = map(int, self.viewer.cget('scrollregion').split()[-2:])
            x0, x1 = bbox[0]/w, bbox[2]/w
            y0, y1 = bbox[1]/h, bbox[3]/h
            xv0, xv1 = self.viewer.xview()
            yv0, yv1 = self.viewer.yview()
            if xv0 > x0:
                self.viewer.xview_moveto(x0)
            elif xv1 < x1:
                self.viewer.xview_moveto(x1 - xv1 + xv0)
            if yv0 > y0:
                self.viewer.yview_moveto(y0)
            elif yv1 < y1:
                self.viewer.yview_moveto(y1 - yv1 + yv0)

    def _display_current_search_result(self):
        page, nb = self._search_results_list[self._search_index]
        if page == self.current_page:
            self.viewer.itemconfigure('search_current', outline='red', width=1)
            self.viewer.dtag('search_current', 'search_current')
            self.viewer.itemconfigure(f'search#{nb}', outline='cyan', width=2)
            self.viewer.addtag_withtag('search_current', f'search#{nb}')
        else:
            self.view_page(page)
        self._see_current_search_result()

    def _display_next(self):
        if not self._search_results_list:
            showinfo(_("Search"), _("No result found."))
        else:
            try:
                self._search_index = self._search_results_list.index((self.current_page, 0))
            except ValueError:
                self._search_results_list.append((self.current_page, 0))
                self._search_results_list.sort()
                self._search_index = self._search_results_list.index((self.current_page, 0))
                del self._search_results_list[self._search_index]
                if self._search_index == len(self._search_results_list):
                    self._search_index -= 1
                self.view_page(self._search_results_list[self._search_index][0])
            else:
                self._render_search_results()
            self._see_current_search_result()

    def search_next(self):
        string = self.search_entry.get()
        if self._search_string == string:
            if self._search_index == len(self._search_results_list) - 1:
                showinfo(_("Search"), _("No more results."))
            else:
                self._search_index += 1
            self._display_current_search_result()
        else:
            self._search(string)
            self._display_next()

    # --- zoom
    def _zoom_trace(self, *args):
        self.zoom_entry.delete(0, 'end')
        self.zoom_entry.insert(0, f'{self.zoom.get():g}%')

    def _set_zoom(self, zoom):
        self._fit_zoom.set('')
        self.zoom.set(zoom)
        self.view_page()

    def _validate_zoom(self, event):
        value = self.zoom_entry.get().strip().rstrip('%')
        try:
            self.zoom.set(float(value))
        except ValueError:
            self._reset_zoom()

    def _reset_zoom(self, event=None):
        self.zoom_entry.delete(0, 'end')
        self.zoom_entry.insert(0, f'{self.zoom.get():g}%')

    # --- navigate
    def next_page(self, event=None):
        if self.current_page < len(self.pages):
            self.view_page(self.current_page + 1)
            self.viewer.yview_moveto(0)
            self.viewer.xview_moveto(0)
        return "break"

    def prev_page(self, event=None):
        if self.current_page > 1:
            self.view_page(self.current_page - 1)
            self.viewer.yview_moveto(0)
            self.viewer.xview_moveto(0)
        return "break"

    def _set_page(self, event):
        if not self.pages:
            return
        try:
            pagenb = int(self.page_entry.get())
        except ValueError:
            self.page_entry.delete(0, 'end')
            self.page_entry.insert(0, self.current_page)
        if pagenb < 1:
            pagenb = 1
        elif pagenb > len(self.pages):
            pagenb = len(self.pages)
        self.view_page(pagenb)
        return "break"

    def _on_click(self, event, page):
        if event.widget.identify_element(event.x, event.y) != 'Treeitem.indicator':
            if page != self.current_page:
                self.view_page(page)

    # --- save
    def save(self, event=None):
        if not self.filepath or self.doc.permissions == 0 or not self.doc.can_save_incrementally():
            return
        self.unsaved_changes = False
        self.doc.save(self.filepath, incremental=True,
                      encryption=fitz.PDF_ENCRYPT_KEEP)
        self._mtime = os.stat(self.filepath).st_mtime
        self._file_monitoring_start()
        logging.info("Saved '%s'.", self.filepath)

    def saveas(self, event=None):
        if not self.filepath or self.doc.permissions == 0:
            return

        initialdir, initialfile = os.path.split(self.filepath)

        filename = asksaveasfilename('.pdf',
                                     [('PDF', '*.pdf'), (_('All files'), '*')],
                                     initialdir, initialfile)
        if filename:
            self.doc.save(filename, encryption=fitz.PDF_ENCRYPT_KEEP)
            self.filepath = filename
            self._file_monitoring_start()
            self.unsaved_changes = False

    # --- open
    def _save_recents(self):
        if len(self.recent_files) > 10:
            del self.recent_files[0]
            self.menu_recent_files.delete(0)
        cst.CONFIG.set("General", "recent_files", ", ".join(self.recent_files))
        cst.save_config()

    def open(self, event=None, filename=None):
        ans = False
        if self.unsaved_changes:
            ans = askyesnocancel(_("Confirmation"),
                                 _("There are some unsaved changes to '{filename}'. Do you want to save them before opening another document?").format(filename=self.filepath),
                                 self)
        if ans is None:
            return
        if ans:
            self.save()
        if self.filepath:
            initialdir = os.path.dirname(self.filepath)
        else:
            initialdir = os.path.expanduser('~')
        if filename is None:
            filename = askopenfilename('.pdf',
                                       [('PDF', '*.pdf'), (_('All files'), '*')],
                                       initialdir)
        if filename:
            logging.info("Opening '%s' ...", self.filepath)
            self.open_doc(filename)
            logging.info("Opened '%s'.", self.filepath)

    def open_doc(self, filepath):
        self.configure(cursor='watch')
        self.update_idletasks()
        try:
            self.doc = fitz.open(filepath)
        except RuntimeError:
            self.configure(cursor='')
            abs_path = os.path.abspath(filepath)
            if abs_path in self.recent_files:
                self.recent_files.remove(abs_path)
                self.menu_recent_files.delete(abs_path)
                self._save_recents()
                if not self.recent_files:
                    self._recent_btn.state(['disabled'])
            logging.exception("Failed to open %s", filepath)
            showerror(_("Error"),
                      _("Cannot open {filepath}").format(filepath=filepath),
                      traceback.format_exc(),
                      parent=self)
            return
        self._set_toolbar_state("disabled")
        self.filepath = filepath
        self._mtime = os.stat(self.filepath).st_mtime
        abs_path = os.path.abspath(filepath)
        if abs_path in self.recent_files:
            self.recent_files.remove(abs_path)
            self.menu_recent_files.delete(abs_path)
        self.recent_files.append(abs_path)
        self.menu_recent_files.add_command(label=abs_path,
                                           command=lambda: self.open(filename=abs_path))
        self._save_recents()
        self._recent_btn.state(["!disabled"])

        self._search_reset()

        if self.doc.is_encrypted:
            self._title = os.path.basename(filepath)
            self.pages.clear()
            self._unlock_button.grid(row=0, column=0)
            self._save_btn.state(['disabled'])
            self.viewer.itemconfigure('page', image='')
            self.page_total.configure(text='/0')
            self.current_page = 1
            self.configure(cursor='')
            self.update_idletasks()
            self.unlock_doc()
        else:
            self.load_doc()
        self._file_monitoring_start()

    def reload(self, event=None):
        ans = True
        if self.unsaved_changes:
            ans = askokcancel(_("Confirmation"),
                              _("There are some unsaved changes to '{filename}' that you will loose."
                                " Proceed with the reloading?").format(filename=self.filepath),
                              self)
        if ans:
            logging.info("Reloading '%s' ...", self.filepath)
            self.open_doc(self.filepath)
            logging.info("Reloaded '%s'.", self.filepath)

    def unlock_doc(self):
        pwd = askpassword(message=_('The document "{file}" is locked.').format(file=self.filepath),
                          parent=self)
        if pwd is None:
            return
        res = self.doc.authenticate(pwd)
        if res == 0:
            self.unlock_doc()
        else:
            self._unlock_button.grid_remove()
            logging.info("Unlocked '%s'", self.filepath)
            self.load_doc()

    def load_doc(self):
        self.configure(cursor='watch')
        self.update_idletasks()
        self._title = self.doc.metadata.get('title', '')
        if not self._title:
            self._title = os.path.basename(self.filepath)
        self._set_toolbar_state("!disabled")
        self.unsaved_changes = False
        self.pages = list(self.doc.pages())
        tot = len(self.pages)
        self.page_entry.configure(width=len(str(tot)) + 1)
        self.page_total.configure(text=f'/{tot}')
        self._search_results.update({p+1: [] for p in range(tot)})
        self.populate_toc()
        self.populate_thumbnails()
        self.load_annot()
        self.load_links()
        self.view_page(1)
        self.configure(cursor='')

    def load_links(self):
        self._links.clear()
        for p, page in enumerate(self.pages, 1):
            self._links[p] = []
            for link in page.get_links():
                if link['kind'] == 1:
                    url = ''
                    goto_x, goto_y = link['to']
                    pagenb = link['page']
                    goto = (pagenb, goto_x, goto_y)

                    def action(event, pg=pagenb + 1, target=(goto_x, goto_y)):
                        if self._annotate.get():
                            return
                        self._link_open_internal(pg, target)

                elif link['kind'] == 2:
                    url = link['uri']
                    goto = ()

                    def action(event, u=url):
                        if self._annotate.get():
                            return
                        url_open(u)

                else:
                    continue

                x0, y0, x1, y1 = list(link['from'].irect)
                self._links[p].append(([x0, y0, x1, y0, x1, y1, x0, y1], action, url, goto))

    def load_annot(self):
        for page in self._annots:
            self.annots.delete(*self.annots.get_children(f'Page{page}'))
        self.annots.delete(*self.annots.get_children())
        self._annots.clear()
        annots = {p: list(page.annots())
                  for p, page in enumerate(self.pages, 1)}
        for page, values in annots.items():
            key = f'Page{page}'
            try:
                self.annots.insert('', 'end', key, text=f'Page {page}', tags=key)
            except tk.TclError:
                self.annots.move(key, '', 'end')
            self._annots[page] = {}
            displayed_annot = False
            for annot in values:
                if annot.flags == fitz.PDF_ANNOT_IS_HIDDEN:
                    continue  # do not display annots flagged as hidden
                self._create_annot(annot, page, False)
                displayed_annot = True
            self.annots.tag_bind(key, '<1>',
                                 lambda ev, pg=page: self._on_click(ev, pg))
            if not displayed_annot:
                self.annots.detach(key)

    def _create_annot(self, annot, pagenb, update_sidebar=True, **update_kw):
        """Create annotation."""
        key = f'Page{pagenb}'
        if annot.type[1] == 'Ink':
            bbox = []
            for coords in annot.vertices:
                for x, y in coords:
                    bbox.extend([x, y])
        elif annot.vertices:
            bbox = []
            try:
                for x, y in annot.vertices:
                    bbox.extend([x, y])
            except Exception:
                logging.exception("Annot creation failure %s", annot)
                return
        else:
            rect = list(annot.rect)
            bbox = [rect[0], rect[1], rect[2], rect[1], rect[2], rect[3], rect[0], rect[3]]
        try:
            color = "#%2.2x%2.2x%2.2x" % tuple(int(c*255)
                                               for c in annot.colors['stroke'])
        except TypeError:
            color = 'white'
        atype = annot.type[0]
        if atype == fitz.PDF_ANNOT_FREE_TEXT and not update_kw:
            if 'DA' in self.doc.xref_get_keys(annot.xref):
                match = re.match(r"\(([01]\.?\d*) ([01].?\d*) ([01].?\d*) rg[^\d]*(\d+\.?\d*)\s*Tf",
                                 self.doc.xref_get_key(annot.xref, 'DA')[1])
                if match:
                    update_kw['text_color'] = [float(c) for c in match.groups()[:-1]]
                    update_kw['fill_color'] = annot.colors['stroke']
                    update_kw['fontsize'] = int(float(match.group(4)))

        author = annot.info['title']
        text = annot.info['content']
        xref = annot.xref
        img = f'img_annot-{annot.type[1].lower()}'
        if img not in self.image_names():
            img = 'img_annot'
        self._annots[pagenb][xref] = (bbox, text, atype, update_kw)  # bounding box, annot text, annot type, props
        self.annots.insert(key, 'end', xref, text=author,
                           image=img,
                           tags=[key, f'xref-{xref}'],
                           values=[color, author, text])
        if update_sidebar:
            # display Page item in Annot sidebar treeview
            pages = list(self.annots.get_children(''))
            if key not in pages:
                pages.append(key)
                pages.sort(key=lambda p: int(p[4:]))
                self.annots.move(key, '', pages.index(key))

        self.annots.tag_bind(f'xref-{xref}', '<Double-1>',
                             lambda ev, nb=xref: self._show_popup(nb, event=ev))
        self.annots.tag_bind(f'xref-{xref}', '<3>',
                             lambda ev, p=pagenb, ref=xref: self._show_annot_menu(ev, p, ref))


    def populate_thumbnails(self):
        self._thumbs.clear()
        self.thumbnails.configure(state='normal')
        self.thumbnails.delete('1.0', 'end')
        for nb, page in enumerate(self.pages, 1):
            zoom = 100 / page.rect[2]
            #~print(dir(page))
            #~print(help(page.get_pixmap))
            px = page.get_pixmap(matrix=fitz.Matrix(zoom, zoom), annots=False)
            size = [px.width, px.height]
            thumbnail = Image.new("RGB", (size[0] + 2, size[1] + 2))
            thumbnail.paste(Image.frombytes("RGB", size, px.samples), (1, 1))
            thumb_img = ImageTk.PhotoImage(thumbnail, master=self)
            thumb = ttk.Radiobutton(self.thumbnails, text=nb, image=thumb_img,
                                    value=nb, variable=self._current_page,
                                    command=lambda pg=nb: self.view_page(pg),
                                    compound='top', style='thumb.TRadiobutton')
            self.thumbnails.window_create('end', window=thumb)
            thumb.bind('<4>', lambda ev: self.thumbnails.event_generate('<4>'))
            thumb.bind('<5>', lambda ev: self.thumbnails.event_generate('<5>'))
            #~self.thumbnails.insert('end', '\n')
            self._thumbs.append((thumb, thumb_img))
        self.thumbnails.configure(state='disabled')

    def populate_toc(self):
        self.toc.delete(*self.toc.get_children())
        toc = self.doc.get_toc()

        parent = ''
        prev_level = 1
        counters = {1: 0}
        page = 0
        for level, title, page in toc:
            if level == prev_level:
                counters[level] += 1
                iid = f'{parent}{counters[level]}'
            elif level > prev_level:
                parent = f'{parent}{counters[prev_level]}'
                counters[level] = counters.get(level, 0) + 1
                iid = f'{parent}{counters[level]}'
            else:
                if level == 1:
                    parent = ''
                else:
                    parent = ''.join([str(counters[i]) for i in range(1, level)])
                counters[level] = counters.get(level, 0) + 1
                iid = f'{parent}{counters[level]}'
            self.toc.insert(parent, 'end', iid, text=title, open=level == 1,
                            values=[page], tags=[iid, f'page{page}'])
            prev_level = level
            self.toc.tag_bind(iid, '<1>', lambda ev, pg=page: self._on_click(ev, pg))
        self.toc.column('page', width=11*len(str(page)) + 4)

    # --- view
    def _set_toolbar_state(self, state):
        """Change state of toolbar elements."""
        for widget in self._toolbar_btns:
            widget.state([state])

    def toggle_leftpane(self):
        if self.left_pane.winfo_ismapped():
            self.panes.remove(self.left_pane)
        else:
            self.panes.add(self.left_pane, before=self.panes.panes()[0],
                           minsize=130, width=130)

    def zoom_in(self, event):
        try:
            self.after_cancel(self._zoom_delay)
        except ValueError:
            pass
        self._fit_zoom.set('')
        self.zoom.set(min(int(self.zoom.get()) + 5, 400))
        self._zoom_delay = self.after(10, self.view_page)

    def zoom_out(self, event):
        try:
            self.after_cancel(self._zoom_delay)
        except ValueError:
            pass
        self._fit_zoom.set('')
        self.zoom.set(max(int(self.zoom.get()) - 5, 5))
        self._zoom_delay = self.after(10, self.view_page)

    def _scroll_viewer_x(self, delta, x1):
        bbox = self.viewer.bbox("all")
        self.viewer.xview_scroll(delta, 'units')
        x1_2 = self.viewer.xview()[1]
        dx = int((x1 - x1_2) * (bbox[2] - bbox[0]))
        for popup in self._annot_popups.values():
            popup.move(dx=dx)

    def _scroll_viewer_y(self, delta, y1):
        bbox = self.viewer.bbox("all")
        self.viewer.yview_scroll(delta, 'units')
        y1_2 = self.viewer.yview()[1]
        dy = int((y1 - y1_2) * (bbox[3] - bbox[1]))
        for popup in self._annot_popups.values():
            popup.move(dy=dy)

    def scroll_up(self, event):
        y0, y1 = self.viewer.yview()
        if (y0, y1) != (0., 1.):
            self._scroll_viewer_y(-1, y1)
        else:
            self.prev_page()

    def scroll_down(self, event):
        y0, y1 = self.viewer.yview()
        if (y0, y1) != (0., 1.):
            self._scroll_viewer_y(1, y1)
        else:
            self.next_page()

    def scroll_left(self, event):
        x0, x1 = self.viewer.xview()
        if (x0, x1) != (0., 1.):
            self._scroll_viewer_x(-1, x1)
        else:
            self.prev_page()

    def scroll_right(self, event):
        x0, x1 = self.viewer.xview()
        if (x0, x1) != (0., 1.):
            self._scroll_viewer_x(1, x1)
        else:
            self.next_page()

    def _show_popup(self, xref, editmode=False):
        originx = self.viewer.canvasx(0)
        originy = self.viewer.canvasy(0)
        bbox = self.viewer.bbox(f'annot-{xref}')
        x = int(bbox[2] - originx + self.viewer.winfo_rootx())
        y = int(bbox[1] - originy + self.viewer.winfo_rooty())
        self._annot_popups[str(xref)].display(x, y, editmode)

    # --- render page
    def _render_links(self):
        self.viewer.delete('link')
        state = "disabled" if self._annotate.get() else "normal"

        for i, (link_coords, action, url, goto) in enumerate(self._links[self.current_page]):
            zoom = self.zoom.get() * self._zoom_pt_to_px
            coords = [zoom*c for c in link_coords]
            if not coords:
                continue
            self.viewer.create_polygon(*coords, fill='', outline='', state=state,
                                       tags=['link', f'link#{i}'])
            self.viewer.tag_bind(f'link#{i}', '<1>', action)
            if goto:
                self.preview_tooltips.add_tooltip(f'link#{i}', goto)
            else:
                self.annot_tooltips.add_tooltip(f'link#{i}', url)

    def _rescale_links(self):
        for i, (link_coords, action, url, preview) in enumerate(self._links[self.current_page]):
            zoom = self.zoom.get() * self._zoom_pt_to_px
            coords = [zoom*c for c in link_coords]
            self.viewer.coords(f'link#{i}', *coords)

    def _render_annots(self):
        self.viewer.delete('annot')
        self.viewer.delete('annot-vertex')
        state = "disabled" if self._annotate.get() else "normal"

        for xref, (annot_coords, annot_text, atype, kw) in self._annots[self.current_page].items():
            zoom = self.zoom.get() * self._zoom_pt_to_px
            coords = [zoom*c for c in annot_coords]
            if not coords:
                continue

            if atype in [fitz.PDF_ANNOT_FREE_TEXT, fitz.PDF_ANNOT_CIRCLE]:
                kw = {'activeoutline': 'gray70', 'activedash': (4,)}
            else:
                kw = {}
            tags = ['annot', f'annot-{xref}']
            if atype in self.annot_draggable:
                tags.append("draggable")

                def B1_release(ev, nb=xref):
                    self._annot_B1_release(nb)

            else:

                def B1_release(ev, nb=xref):
                    self._show_popup(nb)

            self.viewer.create_polygon(*coords, fill='', outline='', state=state,
                                       tags=tags, **kw)
            if atype in self.annot_resizable_rect:
                for i, (x, y) in enumerate([(coords[0], coords[1]), (coords[4], coords[5])]):
                    x1, y1, x2, y2 = x - 4, y - 4, x + 4, y + 4
                    iid = f'annot-vertex-{xref}-{i}'
                    self.viewer.create_polygon(x1, y1, x2, y1, x2, y2, x1, y2,
                                               outline='', fill='',
                                               activeoutline='gray70',
                                               activefill='gray90',
                                               tags=['annot-vertex', iid])
                    self.viewer.tag_bind(iid, '<ButtonPress-1>', lambda ev, nb=xref: self._annot_start_drag(ev, f'annot-{xref}'))
                    self.viewer.tag_bind(iid, '<B1-Motion>', lambda ev, nb=xref, v=i: self._annot_resize_drag(ev, nb, v))
                    self.viewer.tag_bind(iid, '<ButtonRelease-1>', lambda ev, nb=xref, v=i: self._annot_resize_drag_stop(nb, v))
            self.viewer.tag_bind(f'annot-{xref}', '<ButtonRelease-1>', B1_release)
            self.viewer.tag_bind(f'annot-{xref}', '<3>',
                                 lambda ev, nb=xref: self._show_annot_menu(ev, self.current_page, nb))
            if annot_text:
                self.annot_tooltips.add_tooltip(f'annot-{xref}', annot_text)

    def _rescale_annots(self):
        for xref, (annot_coords, annot_text, atype, kw) in self._annots[self.current_page].items():
            zoom = self.zoom.get() * self._zoom_pt_to_px
            coords = [zoom*c for c in annot_coords]
            self.viewer.coords(f'annot-{xref}', *coords)
            # Move annot vertices to follow annot resizing / dragging.
            if atype in self.annot_resizable_rect:
                vertices = [(coords[0], coords[1]), (coords[4], coords[5])]
                for i, (x, y) in enumerate(vertices):
                    x1, y1, x2, y2 = x - 4, y - 4, x + 4, y + 4
                    self.viewer.coords(f'annot-vertex-{xref}-{i}', x1, y1, x2, y1, x2, y2, x1, y2)

    def _render_search_results(self):
        self.viewer.delete('search')
        for i, res in enumerate(self._search_results[self.current_page]):
            zoom = self.zoom.get() * self._zoom_pt_to_px
            coords = [c*zoom for c in res]
            self.viewer.create_rectangle(*coords, outline='red',
                                         tags=['search', f'search#{i}'])
        if not self._search_results_list:
            return
        sel = self._search_results_list[self._search_index]
        if sel[0] == self.current_page:
            self.viewer.itemconfigure(f'search#{sel[1]}', outline='cyan', width=2)
            self.viewer.addtag_withtag('search_current', f'search#{sel[1]}')

    def _rescale_search_results(self):
        for i, res in enumerate(self._search_results[self.current_page]):
            zoom = self.zoom.get() * self._zoom_pt_to_px
            coords = [zoom*c for c in res]
            self.viewer.coords(f'search#{i}', *coords)

    def view_page(self, pagenb=None):
        self._add_annot_reset()
        if pagenb:
            self.current_page = pagenb
            self.page_entry.delete(0, 'end')
            self.page_entry.insert(0, pagenb)
            self.annot_tooltips.reset()
            self.annot_tooltips.remove_all()
            for popup in self._annot_popups.values():
                popup.destroy()
            self._annot_popups.clear()
            for xref in self.annots.get_children(f'Page{pagenb}'):
                color, author, text = self.annots.item(xref, 'values')
                self._annot_popups[xref] = Popup(self, color, author, text,
                                                 lambda txt, p=pagenb, i=xref: self._update_annot_text(txt, p, i))

            self._render_links()
            self._render_annots()
            self._render_search_results()
            toc_sections = self.toc.tag_has(f'page{pagenb}')
            if toc_sections:
                i = 0
                while i < len(toc_sections) and not self.toc.bbox(toc_sections[i]):
                    i += 1
                if i < len(toc_sections):
                    self.toc.selection_set(toc_sections[i])
        self.resize_page(force_refresh=True)

    def resize_page(self, event=None, force_refresh=False):
        if not self.pages:
            return
        zoom_fit = self._fit_zoom.get()
        width = self.viewer.winfo_width() - 4
        height = self.viewer.winfo_height() - 4
        if zoom_fit:
            page = self.pages[self.current_page - 1]
            rect = page.bound()

            if zoom_fit == 'width':
                zoom = width / rect[2]
            else:
                r_view = width / height
                r_doc = rect[2] / rect[3]
                if r_view > r_doc:
                    zoom = height / rect[3]
                else:
                    zoom = width / rect[2]
            self.zoom.set(float('{:.2f}'.format(zoom/self._zoom_pt_to_px)))
        elif force_refresh:
            page = self.pages[self.current_page - 1]
            zoom = self.zoom.get() * self._zoom_pt_to_px

        if zoom_fit or force_refresh:
            px = page.get_pixmap(matrix=fitz.Matrix(zoom, zoom))
            self._page = ImageTk.PhotoImage(Image.frombytes("RGB",
                                                            [px.width, px.height],
                                                            px.samples),
                                            master=self)
            self.viewer.itemconfigure('page', image=self._page)
            self._rescale_annots()
            self._rescale_links()
            self._rescale_search_results()

        self.update_idletasks()
        self._page_bbox = self.viewer.bbox('page')
        if not self._page_bbox:
            return
        bbox = list(self._page_bbox)
        if bbox[2] < width:
            dw = (width - bbox[2])//2
            bbox[0] -= dw
            bbox[2] += dw
        if bbox[3] < height:
            dh = (height - bbox[3])//2
            bbox[1] -= dh
            bbox[3] += dh
        self.viewer.configure(scrollregion=bbox)
        self.viewer.coords('bg', *bbox)

    # --- links
    def _link_open_internal(self, page, target):
        """Go to internal link's target."""
        self.view_page(page)
        bbox = self.viewer.cget('scrollregion').split()
        x = target[0] / (int(bbox[2]) - int(bbox[0]))
        y = target[1] / (int(bbox[3]) - int(bbox[1]))
        self.viewer.xview_moveto(x)
        self.viewer.yview_moveto(y)

    def _links_render_preview(self, pagenb, goto_x, goto_y):
        """Return preview around GOTO_Y."""
        page = self.pages[pagenb]
        zoom = self.zoom.get() * self._zoom_pt_to_px

        y1 = int(min(goto_y + 100, page.rect[3]))
        w = self.viewer.winfo_width() / zoom
        x0 = goto_x
        if x0 + w > page.rect[2]:
            x1 = int(page.rect[2])
            x0 = int(x1 - w)
        else:
            x1 = int(goto_x + w)

        px = page.get_pixmap(matrix=fitz.Matrix(zoom, zoom),
                             alpha=False, annots=False,
                             clip=[x0, goto_y, x1, y1])
        im = Image.frombytes("RGB", [px.width, px.height], px.samples)
        return ImageTk.PhotoImage(im, master=self)

    # --- annots
    def _get_page_coords(self, event):
        """Return the canvas coords inside the page the closest to event's coordinates."""
        x = min(max(self.viewer.canvasx(event.x), self._page_bbox[0]), self._page_bbox[2])
        y = min(max(self.viewer.canvasy(event.y), self._page_bbox[1]), self._page_bbox[3])
        return x, y

    # --- ---- browsing mode: right-click menu, edit prop, delete, drag and open popup
    def _annot_prop(self, pagenb, xref):
        """Edit annotation properties."""
        kw = self._annots[pagenb][int(xref)][-1]
        prop = AnnotProps(self, self.pages[pagenb - 1].load_annot(int(xref)), **kw)
        self.wait_window(prop)
        if prop.props:
            self.unsaved_changes = True
            (bbox, text, outlined, kw) = self._annots[pagenb][int(xref)]
            self._annots[pagenb][int(xref)] = (bbox, text, outlined, prop.props[2])
            self.annots.item(str(xref), text=prop.props[1],
                             values=[prop.props[0], prop.props[1], text])
            if pagenb == self.current_page:
                self.view_page(pagenb)

    def _delete_annot(self, pagenb, xref):
        """Delete annotation with given xref on given page."""
        self.annots.delete(str(xref))
        del self._annots[pagenb][int(xref)]
        if str(xref) in self._annot_popups:
            self._annot_popups[str(xref)].destroy()
        del self._annot_popups[str(xref)]
        self.viewer.delete(f'annot-{xref}')
        page = self.pages[pagenb - 1]
        page.delete_annot(page.load_annot(int(xref)))
        if pagenb == self.current_page:
            self.view_page(pagenb)

    def _show_annot_menu(self, event, pagenb, xref):
        """Display right-click annotation menu."""
        self._annot_menu.entryconfigure(_("Delete"), command=lambda: self._delete_annot(pagenb, xref))
        self._annot_menu.entryconfigure(_("Properties"), command=lambda: self._annot_prop(pagenb, xref))
        self._annot_menu.tk_popup(event.x_root, event.y_root)

    def _annot_resize_drag(self, event, xref, vertex):
        """Resize annot by dragging corner with mouse."""
        if not self._annot_drag_pos:
            return
        x, y = self._get_page_coords(event)
        dx, dy = x - self._annot_drag_pos[0], y - self._annot_drag_pos[1]
        self.viewer.move('current', dx, dy)
        coords = self.viewer.coords(f'annot-{xref}')
        if vertex == 0:
            coords[0] += dx
            coords[6] += dx
            coords[1] += dy
            coords[3] += dy
        else:
            coords[2] += dx
            coords[4] += dx
            coords[5] += dy
            coords[7] += dy
        self.viewer.coords(f'annot-{xref}', *coords)
        self._annot_drag_pos = x, y

    def _annot_resize_drag_stop(self, xref, vertex):
        """Called on click release."""
        if self._annot_drag_pos_init == self._annot_drag_pos:  # no dragging
            self._show_popup(xref)
        # end dragging
        self.viewer.configure(cursor='')
        item = f'annot-{xref}'
        x, y = self._annot_drag_pos
        self._annot_drag_pos = ()
        self.viewer.itemconfigure(item, outline='')
        # make sure the polygon vertices are in the order (top left), (top right), (bottom right), (bottom left)
        coords = self.viewer.coords(item)
        x0, y0 = min(coords[::2]), min(coords[1::2])
        x1, y1 = max(coords[::2]), max(coords[1::2])
        coords = [x0, y0, x1, y0, x1, y1, x0, y1]
        self.viewer.coords(item, *coords)

        # set new annot rect
        zoom = self.zoom.get() * self._zoom_pt_to_px
        dx = (x - self._annot_drag_pos_init[0])/zoom
        dy = (y - self._annot_drag_pos_init[1])/zoom
        annot = self.pages[self.current_page - 1].load_annot(int(xref))
        x0, y0, x1, y1 = annot.rect
        x0, y0, x1, y1 = x0 + dx * (1 - vertex), y0 + dy * (1 - vertex), x1 + dx * vertex, y1 + dy * vertex
        annot.set_rect([min(x0, x1), min(y0, y1), max(x0, x1), max(y0, y1)])
        (bbox, text, atype, kw) = self._annots[self.current_page][int(xref)]
        annot.update(**kw)
        bbox = [c/zoom for c in coords]
        self._annots[self.current_page][int(xref)] = (bbox, text, atype, kw)
        self.unsaved_changes = True
        self.resize_page(force_refresh=True)

    def _annot_start_drag(self, event, item='current'):
        """Start dragging annot with mouse."""
        self._annot_drag_pos_init = self.viewer.canvasx(event.x), self.viewer.canvasy(event.y)
        self._annot_drag_pos = self._annot_drag_pos_init
        self.viewer.configure(cursor='fleur')
        self.viewer.itemconfigure(item, outline='grey', dash=(5,))

    def _annot_drag(self, event):
        """Drag annot with mouse."""
        if not self._annot_drag_pos:
            return
        x, y = self._get_page_coords(event)
        self.viewer.move('current', x - self._annot_drag_pos[0], y - self._annot_drag_pos[1])
        self._annot_drag_pos = x, y

    def _annot_B1_release(self, xref):
        """Called on click release."""
        if self._annot_drag_pos_init == self._annot_drag_pos:  # no dragging
            self._show_popup(xref)
        # end dragging
        self.viewer.configure(cursor='')
        x, y = self._annot_drag_pos
        self._annot_drag_pos = ()
        self.viewer.itemconfigure(f'annot-{xref}', outline='')
        zoom = self.zoom.get() * self._zoom_pt_to_px
        dx = (x - self._annot_drag_pos_init[0])/zoom
        dy = (y - self._annot_drag_pos_init[1])/zoom
        annot = self.pages[self.current_page - 1].load_annot(int(xref))
        x0, y0, x1, y1 = annot.rect
        annot.set_rect([x0 + dx, y0 + dy, x1 + dx, y1 + dy])
        (bbox, text, atype, kw) = self._annots[self.current_page][int(xref)]
        bbox = [c/zoom for c in self.viewer.coords(f'annot-{xref}')]
        self._annots[self.current_page][int(xref)] = (bbox, text, atype, kw)
        self.unsaved_changes = True
        self.resize_page(force_refresh=True)

    # --- --- annot mode: drawing
    def _annot_deselect(self):
        """Go back to browsing mode."""
        self.viewer.unbind('<ButtonPress-1>')
        self.viewer.unbind('<ButtonRelease-1>')
        self.viewer.unbind('<B1-Motion>')
        self._add_annot_reset()
        self.viewer.itemconfigure('annot', state='normal')
        self.viewer.itemconfigure('link', state='normal')
        self.unbind('<Escape>')
        self.unbind('<3>')
        self.viewer.configure(cursor='')
        self._page_cursor = ''

    def toggle_add_annot(self, event=None):
        kind = self._annotate.get()
        if not kind:
            return
        self.viewer.itemconfigure('annot', state='disabled')
        self.viewer.itemconfigure('link', state='disabled')
        self.bind('<Escape>', lambda ev: self._annotate.set(""))
        self.bind('<3>', lambda ev: self._annotate.set(""))
        self.viewer.unbind('<ButtonPress-1>')
        self.viewer.unbind('<B1-Motion>')
        self._add_annot_reset()

        if kind in ['line', 'arrow', 'rectangle', 'circle', 'polygon', 'polyline']:
            self.viewer.bind('<ButtonRelease-1>', getattr(self, f"_draw_{kind}"))
        elif kind == "ink":
            self.viewer.bind('<ButtonPress-1>', self._draw_annot_start)
            self.viewer.bind('<B1-Motion>', self._draw_annot_addpoint)
            self.viewer.bind('<ButtonRelease-1>', self._draw_ink_stop)
        elif kind in ['text', 'caret']:
            self.viewer.bind('<ButtonRelease-1>', getattr(self, f"_add_{kind}_annot"))
        elif kind == 'freetext':
            self.viewer.bind('<ButtonRelease-1>', self._draw_freetext)
        else:
            raise NotImplementedError("Unknown annotation type %s" % kind)
        self._page_cursor = self._annot_cursors.get(kind, "")
        self.viewer.configure(cursor=self._page_cursor)

    def _valid_point(self, x, y):
        # don't draw outside image
        if not (self._page_bbox[0] <= x <= self._page_bbox[2]):
            return False
        if not (self._page_bbox[1] <= y <= self._page_bbox[3]):
            return False
        #~# don't draw on link/annot
        #~tags = self.viewer.itemcget('current', 'tags')
        #~return ('annot' not in tags) and ('link' not in tags)
        return True

    def _draw_annot_motion(self, event):
        """Make last point of the current drawing follow the mouse."""
        self._draw[1][-2:] = self._get_page_coords(event)
        self.viewer.coords(self._draw[0], *self._draw[1])

    def _draw_annot_addpoint(self, event):
        """Add the point corresponding to the cursor position to the current drawing."""
        self._draw[1].extend(self._get_page_coords(event))
        self.viewer.coords(self._draw[0], *self._draw[1])

    def _draw_annot_start(self, event):
        """Start annotation drawing"""
        x, y = self.viewer.canvasx(event.x), self.viewer.canvasy(event.y)
        if not self._valid_point(x, y):
            return
        iid = self._annot_drawing[self._annotate.get()](*[x, y, x, y])
        self._draw = iid, [x, y, x, y]

    def _draw_poly(self, event):
        self._draw_annot_start(event)
        if self._draw[0]:
            self.viewer.bind('<Motion>', self._draw_annot_motion)
            self.viewer.bind('<ButtonRelease-1>', self._draw_annot_addpoint)
            self.viewer.bind('<Double-1>', self._draw_poly_stop)

    def _draw_poly_stop(self, event):
        zoom = self.zoom.get() * self._zoom_pt_to_px
        poly = [(x/zoom, y/zoom) for x, y in zip(self._draw[1][::2], self._draw[1][1::2])]
        if poly[-1] == poly[-2]:
            poly.pop(-1)
        if len(poly) < 2:
            self._add_annot_reset()
        else:
            self._add_annot(f'add_{self._annotate.get()}_annot', poly)
        self.after_idle(self.viewer.bind, '<ButtonRelease-1>', self._draw_polyline)

    _draw_polygon = _draw_poly
    _draw_polyline = _draw_poly

    def _draw_ink_stop(self, event):
        zoom = self.zoom.get() * self._zoom_pt_to_px
        points = [(x/zoom, y/zoom) for x, y in zip(self._draw[1][2::2], self._draw[1][3::2])]
        if len(points) < 6:
            self._add_annot_reset()
            return
        self._add_annot('add_ink_annot', [points])

    def _draw_rect(self, event, annot_fct, *extra_args, **extra_kwargs):
        if not self._draw[0]:  # start drawing
            self._draw_annot_start(event)
            self.viewer.bind('<Motion>', self._draw_annot_motion)
        else:  # end drawing
            zoom = self.zoom.get() * self._zoom_pt_to_px
            rect = [c/zoom for c in self._draw[1]]
            if rect[0] == rect[2] or rect[1] == rect[3]:
                self._add_annot_reset()
                return
            x0, y0, x1, y1 = rect
            rect = [min(x0, x1), min(y0, y1), max(x0, x1), max(y0, y1)]
            self._add_annot(annot_fct, rect, *extra_args, **extra_kwargs)

    def _draw_rectangle(self, event):
        self._draw_rect(event, 'add_rect_annot')

    def _draw_circle(self, event):
        self._draw_rect(event, 'add_circle_annot')

    def _draw_freetext(self, event):
        color = [float(v) for v in cst.CONFIG.get('Annot-freetext', 'color').split(",")]
        fill = cst.CONFIG.get('Annot-freetext', 'fill')
        if fill:
            fill = [float(v) for v in fill.split(",")]
        else:
            fill = None
        self._draw_rect(event, 'add_freetext_annot', '',
                        fontsize=cst.CONFIG.getint('Annot-freetext', 'fontsize'),
                        text_color=color, fill_color=fill,
                        align=cst.CONFIG.getint('Annot-freetext', 'align'))

    def _draw_line(self, event):
        if not self._draw[0]:  # start drawing
            self._draw_annot_start(event)
            self.viewer.bind('<Motion>', self._draw_annot_motion)
        else:
            zoom = self.zoom.get() * self._zoom_pt_to_px
            rect = [c/zoom for c in self._draw[1]]
            if rect[0] == rect[2] and rect[1] == rect[3]:
                self._add_annot_reset()
                return
            self._add_annot('add_line_annot', rect[:2], rect[2:])

    _draw_arrow = _draw_line

    def _add_pointlike_annot(self, event, center_offset, *args):
        """
        Add annot where the user clicked.

            * event: tkinter click event
            * center_offset: offset to the annot center
                The point passed to the method to create the annot is the top left
                corner but the user clicked where the center should be.
        """
        x, y = self.viewer.canvasx(event.x), self.viewer.canvasy(event.y)
        if not self._valid_point(x, y):
            return
        zoom = self.zoom.get() * self._zoom_pt_to_px
        page = self.pages[self.current_page - 1]
        x0, y0 = center_offset
        x = min(max(x/zoom - x0, 0), page.rect[2] - 2*x0)
        y = min(max(y/zoom - y0, 0), page.rect[3] - 2*y0)
        self._add_annot(f'add_{self._annotate.get()}_annot', (x, y), *args)

    def _add_caret_annot(self, event):
        self._add_pointlike_annot(event, (9, 7))

    def _add_text_annot(self, event):
        self._add_pointlike_annot(event, (10, 10), '')

    def _add_annot(self, add_annot_fct, *args, **kwargs):
        """
        Add new annotation.

        Arguments:
            * add_annot_fct: str
                name of the method to use to create the annotation
            * args: tuple
                arguments to pass during the annotation creation
        """
        try:
            annot = getattr(self.pages[self.current_page - 1], add_annot_fct)(*args, **kwargs)
        except Exception:
            logging.exception("Error %s: %s", add_annot_fct, str(args))
            self._add_annot_reset()
            return
        cat = f"Annot-{self._annotate.get()}"
        col = cst.CONFIG.get(cat, "color").split(",")
        fill = cst.CONFIG.get(cat, "fill", fallback="")
        if not fill:
            if cat in ["Annot-arrow", "Annot-line"]:
                fill = col
            else:
                fill = []
        else:
            fill = fill.split(",")
        if add_annot_fct != 'add_freetext_annot':
            annot.set_colors(stroke=[float(c) for c in col],
                             fill=[float(c) for c in fill])
        else:
            annot.set_colors(stroke=[float(c) for c in fill])
        width = cst.CONFIG.getfloat(cat, "linewidth", fallback=-1.)
        if width >= 0:
            annot.set_border(width=width)
        annot.set_opacity(cst.CONFIG.getfloat(cat, "opacity"))
        icon = cst.CONFIG.get(cat, "icon", fallback=None)
        if icon:
            annot.set_name(icon)
        ending_left = cst.CONFIG.getint(cat, "ending_left", fallback=None)
        ending_right = cst.CONFIG.getint(cat, "ending_right", fallback=None)
        if ending_right is not None:
            annot.set_line_ends(ending_left, ending_right)
        kwargs.pop('align', None)
        annot.update(**kwargs)

        annot.set_info(title=cst.CONFIG.get("General", "annot_author"))
        self._create_annot(annot, self.current_page, **kwargs)
        self.view_page(self.current_page)
        self._add_annot_reset()
        self.unsaved_changes = True
        if cat.endswith("text"):
            self._show_popup(str(annot.xref), editmode=True)

    def _add_annot_reset(self):
        """Reset annotation temporary drawing."""
        self.viewer.unbind('<Motion>')
        self.viewer.unbind('<Double-1>')
        if self._draw[0]:
            self.viewer.delete(self._draw[0])
            self._draw = 0, []

    def _update_annot_text(self, text, pagenb, xref):
        annot = self.pages[pagenb - 1].load_annot(int(xref))
        old_text = annot.info['content']
        if old_text == text:
            return  # no modification
        annot.set_info(content=text)
        self.annots.set(xref, 'text', text)
        self.annot_tooltips.add_tooltip(f'annot-{xref}', text)
        self.unsaved_changes = True
        (bbox, old_text, is_freetext, kw) = self._annots[pagenb][int(xref)]
        self._annots[pagenb][int(xref)] = (bbox, text, is_freetext, kw)
        if is_freetext:
            annot.update(**kw)
            self.view_page(self.current_page)

    # --- misc
    def show_info(self):
        if not self.doc:
            return
        top = tk.Toplevel(self, padx=4, pady=4)
        top.transient(self)
        ttk.Label(top, text='File:',
                  font=(None, 9, 'bold')).grid(row=0, sticky='e')
        ttk.Label(top, text=self.filepath).grid(row=0, column=1, sticky='w')
        for i, (key, value) in enumerate(self.doc.metadata.items(), 1):
            ttk.Label(top, text=key.capitalize() + ':',
                      font=(None, 9, 'bold')).grid(row=i, sticky='e')
            ttk.Label(top, text=value).grid(row=i, column=1, sticky='w')

    def report_callback_exception(self, *args):
        """Log exceptions."""
        err = "".join(traceback.format_exception(*args))
        logging.error(err)
        if args[0] is not KeyboardInterrupt:
            showerror(_("Error"), str(args[1]), err, True)
        else:
            self.destroy()

    def settings(self):
        """Open settings dialog."""
        dialog = Config(self)
        self.wait_window(dialog)
        for popup in self._annot_popups.values():
            popup.update_style()
        cst.save_config()

    def about(self):
        About(self)

    def quit(self):
        ans = False
        if self.unsaved_changes:
            ans = askyesnocancel(_("Confirmation"),
                                 _("There are some unsaved changes to '{filename}'. Do you want to save them before closing?").format(filename=self.filepath),
                                 self)
        if ans is None:
            return
        if ans:
            self.save()
        self._file_monitoring_stop()
        logging.info('Closing {}'.format(cst.APP_NAME))
        self.destroy()
