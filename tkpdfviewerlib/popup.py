# -*- coding: utf-8 -*-
"""
tkPDFViewer - Basic PDF viewer
Copyright 2020 Juliette Monsel <j_4321@protonmail.com>

tkPDFViewer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tkPDFViewer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Popups
"""
import tkinter as tk
from tkinter import ttk
import re
from webbrowser import open as url_open

from PIL import Image, ImageTk
from PIL.ImageTk import PhotoImage
from matplotlib.mathtext import MathTextParser
import latexcodec

from .constants import APP_NAME, IM_SCROLL_ALPHA, CONFIG
from .autoscrollbar import AutoScrollbar
from .config import OpacityFrame, ColorFrame, IconSelection, LineEndSelection, \
    OptionalColorFrame, AlignSelection


def is_color_light(color):
    r, g, b = color
    p = ((0.299 * r ** 2 + 0.587 * g ** 2 + 0.114 * b ** 2) ** 0.5) / 255
    return p > 0.5


def active_color(color, output='HTML'):
    """Return a lighter shade of color (RGB triplet with value max 255) in HTML format."""
    r, g, b = color
    if is_color_light(color):
        r *= 3 / 4
        g *= 3 / 4
        b *= 3 / 4
    else:
        r += (255 - r) / 3
        g += (255 - g) / 3
        b += (255 - b) / 3
    if output == 'HTML':
        return ("#%2.2x%2.2x%2.2x" % (round(r), round(g), round(b))).upper()
    else:
        return (round(r), round(g), round(b))


class LatexText(tk.Text):

    latex_parser = MathTextParser('bitmap')

    def __init__(self, master=None, **kwargs):
        kwargs.setdefault("font", CONFIG.get("General", "popup_font"))
        kwargs.setdefault('wrap', 'word')
        tk.Text.__init__(self, master, **kwargs)

        self.images = []
        self._link_color = 'blue'
        self._font_size = 9

        self.bind('<1>', lambda e: self.focus_set())

    def _on_enter_link(self, event, tag):
        self.configure(cursor='hand1')
        self.tag_configure(tag, underline=True, underlinefg=self._link_color)

    def _on_leave_link(self, event, tag):
        self.configure(cursor='xterm')
        self.tag_configure(tag, underline=False)


    def _latex(self, txt):
        try:
            return self.latex_parser.to_rgba(txt, color=self['fg'], dpi=90,
                                             fontsize=self._font_size)[0]
        except Exception as e:
            print(type(e), e, txt)

    def set_content(self, txt):
        """Display txt."""
        text_view = '\n'.join([line.encode().decode('latex') for line in txt.splitlines()])
        res = list(text_view)
        matches = []
        self.configure(state='normal')
        self.delete('1.0', 'end')
        # --- latex rendering
        self.images.clear()
        for match in re.finditer(r'\$[^\$]+\$', text_view):
            img_data = self._latex(match.group())
            if img_data is not None:
                matches.append(match.span())
                self.images.append((ImageTk.PhotoImage(Image.fromarray(img_data),
                                                       master=self), match.group()))
        offset = 0
        imgs = {}

        for (start, end), (img, latex) in zip(reversed(matches), reversed(self.images)):
            del res[start: end]
        for (start, end), (img, latex) in zip(matches, self.images):
            imgs[start - offset] = img
            offset += end - start - 1
        self.insert('1.0', ''.join(res))
        for index, img in imgs.items():
            self.image_create('1.{}'.format(index), image=img)
        # --- links
        link_nb = 1
        count = tk.IntVar(self)
        index_start = self.search(r'https?://[^\s<>"]+|www\.[^\s<>"]+', '1.0', 'end', count=count, regexp=True)
        while index_start:
            index_end = self.index(f'{index_start}+{count.get()}c')
            self.tag_add('link', index_start, index_end)
            self.tag_add(f'link#{link_nb}', index_start, index_end)
            url = self.get(index_start, index_end)
            self.tag_bind(f'link#{link_nb}', '<1>', lambda ev, u=url: url_open(u))
            index_start = self.search(r'https?://[^\s<>"]+|www\.[^\s<>"]+', f'{index_end}+1c', 'end', count=count, regexp=True)
            link_nb += 1
        self.configure(state='disabled')


class Popup(tk.Toplevel):
    def __init__(self, master, color, author, text, savecmd=lambda txt: None):
        """
        Base class for showing comments in Popups
        """
        tk.Toplevel.__init__(self, master, class_=APP_NAME, padx=4, bg=color)
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)
        self.withdraw()

        # --- style
        style = ttk.Style(self)
        style.configure(f'{self}.TSizegrip', background=color)

        # --- --- create widgets scrollbar theme
        bg = color
        vmax = self.winfo_rgb('white')[0]
        widget_bg = tuple(int(val / vmax * 255) for val in self.winfo_rgb(bg))
        if is_color_light(widget_bg):
            widget_fg = (0, 0, 0)
        else:
            widget_fg = (255, 255, 255)

        active_bg = active_color(widget_bg)
        active_bg2 = active_color(active_color(widget_bg, 'RGB'))

        slider_alpha = Image.open(IM_SCROLL_ALPHA)
        slider_vert_insens = Image.new('RGBA', (13, 28), widget_bg)
        slider_vert = Image.new('RGBA', (13, 28), active_bg)
        slider_vert.putalpha(slider_alpha)
        slider_vert_active = Image.new('RGBA', (13, 28), widget_fg)
        slider_vert_active.putalpha(slider_alpha)
        slider_vert_prelight = Image.new('RGBA', (13, 28), active_bg2)
        slider_vert_prelight.putalpha(slider_alpha)

        self._im_trough = tk.PhotoImage(width=15, height=15, master=self)
        self._im_trough.put(" ".join(["{" + " ".join([bg] * 15) + "}"] * 15))
        self._im_slider_vert_active = PhotoImage(slider_vert_active,
                                                 master=self)
        self._im_slider_vert = PhotoImage(slider_vert,
                                          master=self)
        self._im_slider_vert_prelight = PhotoImage(slider_vert_prelight,
                                                   master=self)
        self._im_slider_vert_insens = PhotoImage(slider_vert_insens,
                                                 master=self)
        style.element_create(f'{self}.Vertical.Scrollbar.trough', 'image',
                             self._im_trough)
        style.element_create(f'{self}.Vertical.Scrollbar.thumb', 'image',
                             self._im_slider_vert,
                             ('pressed', '!disabled', self._im_slider_vert_active),
                             ('active', '!disabled', self._im_slider_vert_prelight),
                             ('disabled', self._im_slider_vert_insens), border=6,
                             sticky='ns')
        style.layout(f'{self}.Vertical.TScrollbar',
                     [(f'{self}.Vertical.Scrollbar.trough',
                       {'children': [(f'{self}.Vertical.Scrollbar.thumb', {'expand': '1'})],
                        'sticky': 'ns'})])

        style.map(f'{self}.toggle.TCheckbutton',
                  background=[('!selected', color), ('selected', active_bg2)],
                  focuscolor=[('!selected', color), ('selected', active_bg2)],
                  relief=[('!selected', 'flat'), ('selected', 'sunken')])
        self.transient(master)

        self.attributes('-type', 'splash')  # TODO check splash support

        self.minsize(50, 50)


        self.x = None
        self.y = None

        # mode: False='view' (latex rendering and hyperlinks) and True='edit'
        self._mode = tk.BooleanVar(self, False)

        # --- elements
        frame = tk.Frame(self, bg=color)
        tk.Button(frame, image='img_close', bg=color, padx=0, pady=0,
                  bd=0, command=self.withdraw).pack(side='right')
        ttk.Checkbutton(frame, image='img_edit',
                        command=self.toggle_mode,
                        style=f'{self}.toggle.TCheckbutton',
                        variable=self._mode).pack(side='left')
        self.label = tk.Label(frame, text=author, bg=color,
                              anchor='center')
        self.label.pack(side='left', fill='x', expand=True)
        frame.grid(row=0, columnspan=2, padx=4, pady=4, sticky='ew')
        self.text_view = LatexText(self, width=40, height=15)
        self.text_edit = tk.Text(self, width=40, height=15, wrap='word', undo=True)
        self.text_view.set_content(text)
        self.text_edit.insert('1.0', text)
        self.text_edit.edit_reset()
        self.text_view.configure(state='disabled')
        self.text_edit.grid(row=1, column=0, sticky='ewsn', pady=(0, 8))
        self.text_edit.grid_remove()
        self.text_view.grid(row=1, column=0, sticky='ewsn', pady=(0, 8))
        scroll = AutoScrollbar(self, orient='vertical',
                               style=f'{self}.Vertical.TScrollbar',
                               command=self._yview)
        scroll.grid(row=1, column=1, pady=(0, 8), sticky='ns')
        self.text_view.configure(yscrollcommand=scroll.set)
        self.text_edit.configure(yscrollcommand=scroll.set)

        corner = ttk.Sizegrip(self, style=f'{self}.TSizegrip')
        corner.place(relx=1, rely=1, anchor='se', bordermode='outside')

        # --- bindings
        self.label.bind('<ButtonPress-1>', self._start_move)
        self.label.bind('<ButtonRelease-1>', self._stop_move)
        self.label.bind('<B1-Motion>', self._move)

        self.text_edit.bind('<FocusOut>', lambda ev: savecmd(self.text_edit.get('1.0', 'end-1c')))
        self.text_edit.bind('<Unmap>', lambda ev: self.text_edit.edit_reset())

    def update_style(self):
        try:
            self.text_view.configure(font=CONFIG.get("General", "popup_font"))
        except tk.TclError:  # toplevel has been destroyed
            pass

    def toggle_mode(self):
        """Toggle between view and edit modes."""
        if self._mode.get():
            self.text_edit.grid()
            self.text_edit.focus_set()
            self.text_view.grid_remove()
        else:
            self.text_edit.grid_remove()
            #~self.text_edit.edit_reset()
            self.text_view.set_content(self.text_edit.get('1.0', 'end'))
            self.text_view.grid()

    def _yview(self, *args):
        self.text_edit.yview(*args)
        self.text_view.yview(*args)

    def display(self, x, y, editmode=False):
        self.geometry('+{}+{}'.format(x, y))
        self._mode.set(editmode)
        self.toggle_mode()
        self.deiconify()

    def move(self, dx=0, dy=0):
        if not self.winfo_ismapped():
            return
        x, y = self.winfo_rootx(), self.winfo_rooty()
        #~w, h = self.winfo_width(), self.winfo_height()
        x += dx
        y += dy
        # TODO: prevent to go out of window
        self.geometry("+%s+%s" % (x, y))

    def _start_move(self, event):
        self.x = event.x
        self.y = event.y
        self.configure(cursor='fleur')

    def _stop_move(self, event):
        self.x = None
        self.y = None
        self.configure(cursor='arrow')

    def _move(self, event):
        if self.x is not None and self.y is not None:
            deltax = event.x - self.x
            deltay = event.y - self.y
            x = self.winfo_x() + deltax
            y = self.winfo_y() + deltay
            self.geometry("+%s+%s" % (x, y))


class AnnotProps(tk.Toplevel):
    """Dialog to change the properties of an annotation."""

    def __init__(self, master, annot, **update_kw):
        """
        Create dialog.

        Arguments:
            * master: tkinter app
            * annot: fitz.Annot
            * update_kw: Freetext keyword arguments
        """
        tk.Toplevel.__init__(self, master, padx=4, pady=4)
        self.resizable(True, False)
        self.transient(master)
        self.columnconfigure(1, weight=1)
        self.annot = annot
        self._update_kw = update_kw
        self.props = ()
        # --- author
        row = 0
        ttk.Label(self, text=_('Author:')).grid(row=row, column=0, sticky='e',
                                                pady=8, padx=4)
        self.author = ttk.Entry(self)
        self.author.insert(0, annot.info['title'])
        self.author.grid(row=row, column=1, sticky='ew', padx=4)
        # --- Font size
        if 'fontsize' in update_kw:
            self._is_int = self.register(self._only_int)
            ttk.Label(self, anchor='e',
                      text=_('Font size:')).grid(row=row, column=0, sticky='ne',
                                                 padx=4, pady=4)
            self.fontsize = ttk.Entry(self, validate="key",
                                      validatecommand=(self._is_int, "%P"))
            self.fontsize.insert(0, update_kw['fontsize'])
            self.fontsize.grid(row=row, column=1, sticky='ew', padx=4)
        else:
            self.fontsize = None
        # --- align
        self.align = None
        if annot.type[1] == 'FreeText':
            align = None
            if 'Q' in self.master.doc.xref_get_keys(annot.xref):
                try:
                    align = int(self.master.doc.xref_get_key(annot.xref, 'Q')[1])
                except ValueError:
                    pass
            if align is not None:
                row += 1
                ttk.Label(self, anchor='e',
                          text=_('Text alignment:')).grid(row=row, column=0, sticky='ne',
                                                          padx=4, pady=4)
                self.align = AlignSelection(self, align)
                self.align.grid(row=row, column=1, sticky='ew', padx=4)
        # --- opacity
        row += 1
        ttk.Label(self, text=_('Opacity:')).grid(row=row, column=0, sticky='e',
                                                 pady=8, padx=4)
        val = annot.opacity
        if val < 0:
            val = 1
        self.opacity_scale = OpacityFrame(self, val)
        self.opacity_scale.grid(row=row, column=1, pady=4, sticky='ew')

        # --- line width
        width = annot.border['width']
        if width >= 0:
            row += 1
            ttk.Label(self, text=_('Line width:')).grid(row=row, column=0, sticky='e',
                                                        pady=8, padx=4)
            self._is_float = self.register(self._only_float)
            self.line_width = ttk.Entry(self, validatecommand=(self._is_float, '%P'),
                                        validate='key')
            self.line_width.insert(0, width)
            self.line_width.grid(row=row, column=1, padx=4, sticky='ew')
        else:
            self.line_width = None

        # --- line endings
        if annot.type[1] == 'Line':
            left, right = annot.line_ends
            row += 1
            ttk.Label(self, text=_('Line start:')).grid(row=row, column=0, sticky='e',
                                                        pady=8, padx=4)
            self.line_start = LineEndSelection(self, left)
            self.line_start.grid(row=row, column=1, padx=4, sticky='ew')
            row += 1
            ttk.Label(self, text=_('Line end:')).grid(row=row, column=0, sticky='e',
                                                        pady=8, padx=4)
            self.line_end = LineEndSelection(self, right)
            self.line_end.grid(row=row, column=1, padx=4, sticky='ew')
        else:
            self.line_start = None
            self.line_end = None


        # --- color
        row += 1
        ttk.Label(self, text=_('Color:')).grid(row=row, column=0, sticky='e',
                                               pady=8, padx=4)
        if 'text_color' in update_kw:
            try:
                color = "#%2.2x%2.2x%2.2x" % tuple(int(c*255)
                                                   for c in update_kw['text_color'])
            except TypeError:
                color = 'white'
        else:
            try:
                color = "#%2.2x%2.2x%2.2x" % tuple(int(c*255)
                                                   for c in annot.colors['stroke'])
            except TypeError:
                color = '#ffffff'
        self.color = ColorFrame(self, color)
        self.color.grid(row=row, column=1, sticky='ew', padx=4)

        # --- fill color
        if annot.type[1] in ['Square', 'Circle', 'Polygon', 'FreeText']:
            row += 1
            if 'fill_color' in update_kw:
                fill = update_kw['fill_color']
            else:
                fill = annot.colors['fill']
            var_fill = tk.BooleanVar(self, bool(fill))
            if fill:
                fill = "#%2.2x%2.2x%2.2x" % tuple(int(v*255) for v in fill)
            else:
                fill = "#ffffff"
            self.fill = OptionalColorFrame(self, var_fill, fill)
            ttk.Checkbutton(self, variable=var_fill, command=self.fill.toggle_state,
                            text=_('Fill:')).grid(row=row, column=0, sticky='e',
                                                  padx=4, pady=4)
            self.fill.grid(row=row, column=1, sticky='ew', padx=4)
        else:
            self.fill = None

        # --- icon (text annot only)
        if annot.type[1] == 'Text':
            row += 1
            ttk.Label(self, text=_('Icon:')).grid(row=row, column=0, sticky='e',
                                                  pady=8, padx=4)
            self.icon = IconSelection(self, _(annot.info['name']))
            self.icon.grid(row=row, column=1, sticky='ew', padx=4)
        btn_frame = ttk.Frame(self)
        btn_frame.columnconfigure(0, weight=1)
        btn_frame.columnconfigure(1, weight=1)
        # --- ok/cancel
        row += 1
        ttk.Button(btn_frame, text=_('Ok'),
                   command=self.validate).grid(row=row, column=0, sticky='ew', padx=4)
        ttk.Button(btn_frame, text=_('Cancel'),
                   command=self.destroy).grid(row=row, column=1, sticky='ew', padx=4)
        btn_frame.grid(row=row, columnspan=2, sticky='ew', pady=8)

        self.grab_set()

    @staticmethod
    def _only_int(txt):
        return txt == '' or txt.isdigit()

    @staticmethod
    def _only_float(txt):
        return txt == '' or bool(re.match(r'^\d+\.?\d*$', txt))

    def validate(self):
        if self.line_width:
            self.annot.set_border(width=float(self.line_width.get()))
        if self.line_start:
            self.annot.set_line_ends(self.line_start.get(), self.line_end.get())
        if self.annot.type[1] == 'Text':
            icon = self.icon.get()
            name = self.annot.info['name']
            if icon == 'Unknown' and _(name) not in self.icon.icons:
                icon = name
            self.annot.set_name(icon)
        auth = self.author.get()
        self.annot.set_info(title=auth)
        col, col_rgb = self.color.get_color()
        if 'text_color' in self._update_kw or self.annot.type[1] == 'FreeText':
            self._update_kw['text_color'] = col_rgb
        if self.fill:
            fill, fill_rgb = self.fill.get_color()
            if 'fill_color' in self._update_kw:
                self._update_kw['fill_color'] = fill_rgb
        elif self.line_start:
            fill_rgb = col_rgb
        else:
            fill_rgb = []
        if 'fontsize' in self._update_kw:
            self._update_kw['fontsize'] = int(self.fontsize.get())
        if self.annot.type[1] == 'FreeText':
            self.annot.set_colors(stroke=fill_rgb)
        else:
            self.annot.set_colors(stroke=col_rgb, fill=fill_rgb)
        if self.align:
            self.master.doc.xref_set_key(self.annot.xref, 'Q', str(self.align.get()))
        self.props = (col, auth, self._update_kw)
        opacity = self.opacity_scale.get_opacity()
        self.annot.update(opacity=opacity, **self._update_kw)
        self.destroy()
