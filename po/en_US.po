# tkPDFViewer - Basic PDF viewer
# Copyright (C) 2018 Juliette Monsel <j_4321@protonmail.com>
# This file is distributed under the same license as the tkPDFViewer package.
# Juliette Monsel <j_4321@protonmail.com>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: Juliette Monsel <j_4321@protonmail.com>\n"
"POT-Creation-Date: 2018-11-07 16:19+0100\n"
"PO-Revision-Date: 2018-11-14 16:02+0100\n"
"Last-Translator: Unknown <juliette@Juliette-MacBook>\n"
"Language-Team: English\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: tkpdfviewerlib/version_check.py:65
msgid "Update"
msgstr "Update"

#: tkpdfviewerlib/version_check.py:82
#, python-brace-format
msgid ""
"A new version of {app_name} is available.\n"
"Do you want to download it?"
msgstr ""
"A new version of {app_name} is available.\n"
"Do you want to download it?"

#: tkpdfviewerlib/version_check.py:86 tkpdfviewerlib/messagebox.py:205 tkpdfviewerlib/messagebox.py:267
msgid "Yes"
msgstr "Yes"

#: tkpdfviewerlib/version_check.py:88 tkpdfviewerlib/messagebox.py:206 tkpdfviewerlib/messagebox.py:267
msgid "No"
msgstr "No"

#: tkpdfviewerlib/version_check.py:91
msgid "Check for updates on startup."
msgstr "Check for updates on startup."

#: tkpdfviewerlib/about.py:36
#, python-brace-format
msgid "About {app_name}"
msgstr "About {app_name}"

#: tkpdfviewerlib/about.py:42
msgid "Basic PDF viewer"
msgstr "Basic PDF viewer"

#: tkpdfviewerlib/about.py:45 tkpdfviewerlib/about.py:67
msgid "License"
msgstr "License"

#: tkpdfviewerlib/about.py:47 tkpdfviewerlib/about.py:93
msgid "Close"
msgstr "Close"

#: tkpdfviewerlib/about.py:76
#, python-brace-format
msgid ""
"{app_name} is free software: you can redistribute it and/or modify it under "
"the terms of the GNU General Public License as published by the Free "
"Software Foundation, either version 3 of the License, or (at your option) "
"any later version."
msgstr ""
"{app_name} is free software: you can redistribute it and/or modify it under "
"the terms of the GNU General Public License as published by the Free "
"Software Foundation, either version 3 of the License, or (at your option) "
"any later version."

#: tkpdfviewerlib/about.py:78
#, python-brace-format
msgid ""
"{app_name} is distributed in the hope that it will be useful, but WITHOUT ANY "
"WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS "
"FOR A PARTICULAR PURPOSE. See the GNU General Public License for more "
"details."
msgstr ""
"{app_name} is distributed in the hope that it will be useful, but WITHOUT ANY "
"WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS "
"FOR A PARTICULAR PURPOSE. See the GNU General Public License for more "
"details."

#: tkpdfviewerlib/about.py:80
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program. If not, see http://www.gnu.org/licenses/."
msgstr ""
"You should have received a copy of the GNU General Public License along with "
"this program. If not, see http://www.gnu.org/licenses/."

#: tkpdfviewerlib/messagebox.py:32 tkpdfviewerlib/messagebox.py:95 tkpdfviewerlib/messagebox.py:324
#: tkpdfviewerlib/messagebox.py:392 tkpdfviewerlib/messagebox.py:394
msgid "Ok"
msgstr "Ok"

#: tkpdfviewerlib/messagebox.py:178
msgid "Please report this bug on "
msgstr "Please report this bug on "

#: tkpdfviewerlib/messagebox.py:267 tkpdfviewerlib/messagebox.py:392
msgid "Cancel"
msgstr "Cancel"

#: tkpdfviewerlib/app.py:72
msgid "About"
msgstr "About"

#: tkpdfviewerlib/app.py:75
msgid "Language"
msgstr "Language"

#: tkpdfviewerlib/app.py:76
msgid "Help"
msgstr "Help"

#: tkpdfviewerlib/app.py:92
msgid "Information"
msgstr "Information"

#: tkpdfviewerlib/app.py:93
msgid "The language setting will take effect after restarting the application"
msgstr "The language setting will take effect after restarting the application"

#: tkpdfviewerlib/constants.py:316
msgid "Open"
msgstr "Open"

#: tkpdfviewerlib/constants.py:362
msgid "Save As"
msgstr "Save As"
